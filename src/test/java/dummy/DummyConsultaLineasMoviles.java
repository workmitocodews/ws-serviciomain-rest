package dummy;

import app.serviciomain.bean.MovilLinea;
import app.serviciomain.bean.MovilLineaResponse;
import app.serviciomain.exception.DBException;
import app.serviciomain.util.AbstractRepository;

import java.io.Serializable;
import java.util.ArrayList;

public class DummyConsultaLineasMoviles extends AbstractRepository implements Serializable {

	private static final long serialVersionUID = 1L;

	public MovilLineaResponse dummyConsultaBolsaMoviles(String idTransaccion) throws DBException {
		MovilLineaResponse response = new MovilLineaResponse();
			response.setCodigoRespuesta("0");
			response.setMensajeRespuesta("PROCESO EXITOSO");

			ArrayList<MovilLinea> lstCursor = new ArrayList<>();
			MovilLinea row = new MovilLinea();

			//LINEA;ESTADO_CONTRATO;;BLOQ_SUSP;TIPO_PLAN;CICLO;PO;BOLSA;CO_ID;FAVORITA

			row = this.fila("989479837;a;;FULL EMPRESA RED i 35;6;;;"+14702672+";0");
			lstCursor.add(row);
			
			response.setListaMovilLinea(lstCursor);

		return response;
	}

	private MovilLinea fila(String registro) {

		String[] parts = registro.split(";");

		MovilLinea row = new MovilLinea();
		
		//LINEA;ESTADO_CONTRATO;;BLOQ_SUSP;TIPO_PLAN;CICLO;PO;BOLSA;CO_ID;FAVORITA

		row.setBloqueSuspendido(parts[2]);
		row.setBolsa(parts[6]);
		//row.setCoId(parts[7]);
		row.setEstadoContrato(parts[1]);
		row.setFavorita(parts[8]);
		row.setLinea(parts[0]);
		//row.setPo(parts[5]);
		row.setTipoPlan(parts[3]);
		
		return row;
	}
}
