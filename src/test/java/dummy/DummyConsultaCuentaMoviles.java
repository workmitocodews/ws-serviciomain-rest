package dummy;

import app.serviciomain.bean.Movil;
import app.serviciomain.bean.MovilResponse;
import app.serviciomain.exception.DBException;
import app.serviciomain.util.AbstractRepository;
import app.serviciomain.util.Utilitario;

import java.io.Serializable;
import java.util.ArrayList;

public class DummyConsultaCuentaMoviles extends AbstractRepository implements Serializable {

	private static final long serialVersionUID = 1L;

	public MovilResponse dummyConsultaBolsaMoviles(final String idTransaccion) throws DBException {
		MovilResponse response = new MovilResponse();
		Integer totalCuentas = 0;
		Integer totalLineas = 0;
		Integer totalBolsas = 0;
		Integer totalPlanes = 0;
			response.setCodigoRespuesta("0");
			response.setMensajeRespuesta("PROCESO EXITOSO");

			ArrayList<Movil> lstCursor = new ArrayList<>();
			Movil row = new Movil();

			row = this.fila("16057;3.613.00.00.100002;2;839;9;0");

			totalCuentas++;
			totalLineas = totalLineas + row.getLineas();
			totalBolsas = totalBolsas + row.getBolsa();
			totalPlanes = totalPlanes + row.getPlanes();

			lstCursor.add(row);

			row = this.fila("138725;3.255.27.00.100046;2;27;2;0");

			totalCuentas++;
			totalLineas = totalLineas + row.getLineas();
			totalBolsas = totalBolsas + row.getBolsa();
			totalPlanes = totalPlanes + row.getPlanes();

			lstCursor.add(row);

			row = this.fila("1219794;3.613.00.00.100006;2;3;1;0");

			totalCuentas++;
			totalLineas = totalLineas + row.getLineas();
			totalBolsas = totalBolsas + row.getBolsa();
			totalPlanes = totalPlanes + row.getPlanes();

			lstCursor.add(row);

			row = this.fila("5371423;7.2218213.00.00.100000;2;1;1;0");

			totalCuentas++;
			totalLineas = totalLineas + row.getLineas();
			totalBolsas = totalBolsas + row.getBolsa();
			totalPlanes = totalPlanes + row.getPlanes();

			lstCursor.add(row);

			row = this.fila("14733018;1.10802306;2;1;1;0");

			totalCuentas++;
			totalLineas = totalLineas + row.getLineas();
			totalBolsas = totalBolsas + row.getBolsa();
			totalPlanes = totalPlanes + row.getPlanes();

			lstCursor.add(row);

			response.setTotalCuentas(totalCuentas);
			response.setTotalLineas(totalLineas);
			response.setTotalBolsas(totalBolsas);
			response.setTotalPlanes(totalPlanes);
			response.setListaCuentasMoviles(lstCursor);

		return response;
	}

	private Movil fila(String registro) {

		String[] parts = registro.split(";");

		Movil row = new Movil();

		row.setCustomerId(parts[0]);
		row.setCustcode(parts[1]);
		row.setIdType(Utilitario.stringToInt(parts[2]));
		row.setLineas(Utilitario.stringToInt(parts[3]));
		row.setPlanes(Utilitario.stringToInt(parts[4]));
		row.setBolsa(Utilitario.stringToInt(parts[5]));

		return row;
	}

}
