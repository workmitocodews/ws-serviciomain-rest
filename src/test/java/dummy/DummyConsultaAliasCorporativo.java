package dummy;

import app.serviciomain.bean.Alias;
import app.serviciomain.bean.AliasResponse;
import app.serviciomain.exception.DBException;
import app.serviciomain.util.AbstractRepository;

import java.io.Serializable;
import java.util.ArrayList;

public class DummyConsultaAliasCorporativo extends AbstractRepository implements Serializable {

	private static final long serialVersionUID = 1L;

	public AliasResponse dummyConsultaAliasCorporativo() throws DBException {

		AliasResponse response = new AliasResponse();
		String nombreSP = "PKG_CLIENTE_CORPORATIVO.CUNCSS_LISTA_ALIAS_CORP";

		try {

			response.setCodigoRespuesta("0");
			response.setMensajeRespuesta("PROCESO EXITOSO");

			ArrayList<Alias> lstCursor = new ArrayList<>();
			Alias row = this.fila("16057;3.613.00.00.100002;Pepito1");
			lstCursor.add(row);

			row = this.fila("11779064;3.255.27.00.100046;Pepito2");
			lstCursor.add(row);

			row = this.fila("11795339;3.613.00.00.100006;Pepito3");
			lstCursor.add(row);

			row = this.fila("11831290;7.2218213.00.00.100000;Pepito4");
			lstCursor.add(row);

			row = this.fila("17761048;1.10802306;Pepito5");
			lstCursor.add(row);

			row = this.fila("1776354;8.90.86.00.900807;Pepito6");
			lstCursor.add(row);

			row = this.fila("20060144;8.90.86.00.909689;Pepito7");
			lstCursor.add(row);

			row = this.fila("367170;7.800.70.00.900096;Pepito8");
			lstCursor.add(row);

			row = this.fila("3917935;7.8760990.90.00.900008;Pepito10");
			lstCursor.add(row);

			response.setListaAliasCorporativo(lstCursor);
		}catch (Exception e2) {
			manageToDBException(e2, "BSCS", nombreSP);
		}

		return response;
	}

	private Alias fila(String registro) {

		String[] parts = registro.split(";");

		Alias row = new Alias();

		row.setDescripcion(parts[0]);
		row.setCustcode(parts[1]);
		row.setAlias(parts[2]);

		return row;
	}
}
