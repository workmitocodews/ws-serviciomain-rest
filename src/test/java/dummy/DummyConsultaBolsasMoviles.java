package dummy;

import app.serviciomain.bean.MovilResumen;
import app.serviciomain.bean.MovilResumenResponse;
import app.serviciomain.exception.DBException;
import app.serviciomain.util.AbstractRepository;
import app.serviciomain.util.Utilitario;

import java.io.Serializable;
import java.util.ArrayList;

public class DummyConsultaBolsasMoviles extends AbstractRepository implements Serializable {

	private static final long serialVersionUID = 1L;

	public MovilResumenResponse dummyConsultaBolsaMoviles(String idTransaccion) throws DBException {
		MovilResumenResponse response = new MovilResumenResponse();
			response.setCodigoRespuesta("0");
			response.setMensajeRespuesta("PROCESO EXITOSO");

			ArrayList<MovilResumen> lstCursor = new ArrayList<>();
			MovilResumen row = new MovilResumen();

			row = this.fila("16057;3.613.00.00.100002;839;838;9;Bolsa CDI Premium;0;Bolsa CDI Premium;3;835;0");
			row.setLineasAsociadas(row.getLineasActivas() + row.getLineasBloqueadas() + row.getLineasSuspendidas());
			lstCursor.add(row);

			row = this.fila("16057;3.613.00.00.100002;839;1;9;Claro Exacto;0;Bolsa CDI Premium;0;1;0");
			row.setLineasAsociadas(row.getLineasActivas() + row.getLineasBloqueadas() + row.getLineasSuspendidas());
			lstCursor.add(row);

			response.setListaMovilResumen(lstCursor);

		return response;
	}

	private MovilResumen fila(String registro) {

		String[] parts = registro.split(";");

		MovilResumen row = new MovilResumen();

		row.setCustomerId(parts[0]);
		row.setCustcode(parts[1]);
		row.setLineas(Utilitario.stringToInt(parts[2]));
		row.setLineasPorPlan(parts[3]);
		row.setPlanes(Utilitario.stringToInt(parts[4]));
		row.setDesServicio(parts[5]);
		row.setBolsa(Utilitario.stringToInt(parts[6]));
		row.setNomBolsa(parts[7]);
		row.setLineasActivas(Utilitario.stringToInt(parts[8]));
		row.setLineasBloqueadas(Utilitario.stringToInt(parts[9]));
		row.setLineasSuspendidas(Utilitario.stringToInt(parts[10]));

		return row;
	}

}
