package dummy;

import app.serviciomain.bean.DefaultServiceResponse;
import app.serviciomain.exception.DBException;
import app.serviciomain.types.ServicioAliasResponse;
import app.serviciomain.util.AbstractRepository;

import java.io.Serializable;

public class DummyConsultaServicioAlias extends AbstractRepository implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	public ServicioAliasResponse dummyConsultaServicioAlias(final String idTransaccion) throws DBException {
		
		ServicioAliasResponse response = new ServicioAliasResponse();
		DefaultServiceResponse defaultService = new DefaultServiceResponse(); 

		defaultService.setIdRespuesta("0");
		defaultService.setMensaje("PROCESO EXITOSO");
	
		response.setDefaultServiceResponse(defaultService);

		response.setCuentaAlias("AliasRenzo6tyu");
		response.setDescripcionCuenta("6.150964.00.00.100000");
		response.setServicioAlias("AliasNumCel");
		
		return response;
	}
}
