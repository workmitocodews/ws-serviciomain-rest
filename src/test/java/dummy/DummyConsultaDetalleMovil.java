package dummy;

import app.serviciomain.bean.DefaultServiceResponse;
import app.serviciomain.exception.DBException;
import app.serviciomain.types.DetalleMovilResponse;
import app.serviciomain.util.AbstractRepository;

import java.io.Serializable;

public class DummyConsultaDetalleMovil extends AbstractRepository implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	public DetalleMovilResponse dummyConsultaDetalleMovil(final String idTransaccion) throws DBException {
		
		DetalleMovilResponse response = new DetalleMovilResponse();
		DefaultServiceResponse defaultServiceResponse = new DefaultServiceResponse();
		
		defaultServiceResponse.setIdRespuesta("0");
		defaultServiceResponse.setMensaje("PROCESO EXITOSO");
		
		response.setDefaultServiceResponse(defaultServiceResponse);
		response.setEstado("ACTIVO");
		response.setCustomerID("830416");
		response.setCuenta("830416.333");
		response.setNombrePlan("Plan i10 SP");
		response.setCodigoPlan("1754");
		response.setSimCard("8951101330054404749");
		response.setFechaRenovacion("26/10/2019");
		response.setRoaming("DESACTIVO");
		response.setNumeroDocumento("20100053455");
			
		return response;
	}

}
