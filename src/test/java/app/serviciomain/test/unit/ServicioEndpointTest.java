package app.serviciomain.test.unit;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import app.serviciomain.bean.DefaultServiceResponse;
import app.serviciomain.bussiness.*;
import app.serviciomain.exception.BaseException;
import app.serviciomain.exception.DBException;
import app.serviciomain.exception.WSException;
import app.serviciomain.rest.Servicio;
import app.serviciomain.types.*;
import app.serviciomain.util.Constantes;
import app.serviciomain.util.PropertiesExternos;

import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;

import static org.junit.Assert.assertEquals;

public class ServicioEndpointTest {
	private String idTransaccion="";
	@InjectMocks
	Servicio servicioEndPoint;
	
	@Mock
	HttpHeaders httpHeaders;
	

	@Mock
	private ServicioMovilLineasBusiness servicioMovilLineaBusiness;
	
	@Mock
	private ServicioMovilBusiness servicioMovilBusiness;

	@Mock
	private ServicioMovilResumenBusiness servicioMovilResumenBusiness;

	@Mock
	private FavoritoBusiness favoritoBusiness;
	
	private String numeroDocumento="41643957";
	private String tipoDocumento="01";
	private String numeroMovil="991733567";
	private String numeroCuenta="";
	private String nivel="";
	@Before
	public void init() {
		servicioEndPoint = new Servicio();
		//httpHeaders = Mockito.mock(HttpHeaders.class);
		MockitoAnnotations.initMocks(this);		
		idTransaccion = obtenetIdTransaccion();
	}
	

	private DefaultServiceResponse getDefaultServiceResponse(ServicioRequestBase request) {
		DefaultServiceResponse defaultServiceResponse = new DefaultServiceResponse();
		defaultServiceResponse.setIdRespuesta("0");
		defaultServiceResponse.setIdTransaccional(request.getIdTransaccion());
		defaultServiceResponse.setMensaje("exito");
		defaultServiceResponse.setExcepcion("");
		return defaultServiceResponse;
	}
	/*
	@Test
	public void serviciosContratadosFail() throws DBException, WSException {		
		ServicioRequestBase request = getServicioBaseRequest();
		getHttpHeader();
		Mockito.when(serviciosContratadosBusiness.serviciosContratados(Mockito.any(ServicioRequestBase.class))).thenThrow(WSException.class);
		servicio.serviciosContratados(request);
		assertEquals("1", PropertiesExternos.CODIGO_ERROR_IDF1);
	}
		*/
	@Test
	public void debeInvocarServicioMovilBusinessOk() throws DBException, WSException {		
		ServicioRequestBase request = getServicioBaseRequest();
		
		Mockito.when(servicioMovilBusiness.servicioMovil(Mockito.any(ServicioRequestBase.class)))
		.thenReturn(crearServicioMovilResponse(idTransaccion));
		
		servicioEndPoint.serviciosMoviles(request);
		Mockito.verify(servicioMovilBusiness).servicioMovil(Mockito.any(ServicioRequestBase.class));
	}
	@Test
	public void debeDevolverServicioMovilBusinessErrorIDF01() throws DBException, WSException {		
		ServicioRequestBase request = getServicioBaseRequest();
		request.setNumeroDocumento("");
		request.setTipoDocumento("");
		Response r=servicioEndPoint.serviciosMoviles(request);
		//Mockito.verify(servicioMovilBusiness).servicioMovil(Mockito.any(ServicioRequestBase.class));
		assertEquals("Id respuesta esperada IDF1", PropertiesExternos.CODIGO_ERROR_IDF1, ((ServicioMovilResponse) r.getEntity()).getDefaultServiceResponse().getIdRespuesta());

	}
	@Test
	public void debeInvocarServicioDetalleMovilBusinessOk() throws DBException, WSException {		
		//DetalleMovilRequest request = getDetalleMovilRequest();
		numeroDocumento="41643957";
		tipoDocumento="01";
		numeroMovil="991733567";
		Mockito.when(servicioMovilBusiness.servicioDetalleMovil(Mockito.any(DetalleMovilRequest.class)))
		.thenReturn(Mockito.any(DetalleMovilResponse.class));
		
		servicioEndPoint.consultarDetalleMovil(numeroDocumento,tipoDocumento,numeroMovil);
		Mockito.verify(servicioMovilBusiness).servicioDetalleMovil(Mockito.any(DetalleMovilRequest.class));
	}
	@Test
	public void debeDevolverServicioDetalleMovilBusinessErrorIDF01() throws DBException, WSException {		
		//DetalleMovilRequest request = getDetalleMovilRequest();
		numeroDocumento="";
		tipoDocumento="";
		numeroMovil="";
		Response r=servicioEndPoint.consultarDetalleMovil(numeroDocumento,tipoDocumento,numeroMovil);
		//Mockito.verify(servicioMovilBusiness).servicioMovil(Mockito.any(ServicioRequestBase.class));
		assertEquals("Id respuesta esperada IDF1", PropertiesExternos.CODIGO_ERROR_IDF1, ((DetalleMovilResponse) r.getEntity()).getDefaultServiceResponse().getIdRespuesta());

	}


	private ServicioRequestBase getServicioBaseRequest() {
		ServicioRequestBase request = new ServicioRequestBase();
		request.setNumeroDocumento("123455");
		request.setTipoDocumento("001");
		request.setIdTransaccion("idtx");
		return request;
	}
	private DetalleMovilRequest getDetalleMovilRequest() {
		DetalleMovilRequest request = new DetalleMovilRequest();
		request.setNumeroDocumento("123455");
		request.setTipoDocumento("001");
		request.setIdTransaccion("idtx");
		return request;
	}
	private ServicioMovilResumenRequest getServicioMovilResumenRequest() {
		ServicioMovilResumenRequest request = new ServicioMovilResumenRequest();
		request.setNumeroDocumento("123455");
		request.setTipoDocumento("001");
		request.setIdTransaccion("idtx");
		return request;
	}

	private RegistraLineaFavoritaRequest getRegistraLineaFavoritaRequest() {
		RegistraLineaFavoritaRequest request = new RegistraLineaFavoritaRequest();
		request.setNumeroDocumento("56435678");
		request.setTipoDocumento("001");
		request.setIdTransaccion("idtx");
		request.setNumeroMovil("991733567");
		request.setNumeroCuenta("3.190.00.00.100000");
		return request;
	}
	
	
/*		private void verReferencia(ServicioRequestBase request) {
			request.setNumeroDocumento("41643957");
		}*/
	public String obtenetIdTransaccion() {		
		String strIdTransaccion = "99db31db-9989-4e54-a1be-b68ac0e3exxx";
		Mockito.when(httpHeaders.getHeaderString(Constantes.IDTRANSACCION)).thenReturn(strIdTransaccion);	
		return strIdTransaccion;
	}
	private ServicioMovilResponse crearServicioMovilResponse(String strIdTransaccion) {
		ServicioMovilResponse response = new ServicioMovilResponse();
		DefaultServiceResponse defaultServiceResponse = new DefaultServiceResponse();
		defaultServiceResponse.setIdRespuesta(PropertiesExternos.CODIGO_EXITO);
		defaultServiceResponse.setMensaje(PropertiesExternos.MENSAJE_EXITO);
		defaultServiceResponse.setExcepcion(null);
		defaultServiceResponse.setIdTransaccional(strIdTransaccion);
		response.setDefaultServiceResponse(defaultServiceResponse);
		return response;
	}


	private ServicioMovilLineasResponse crearServicioMovilLineasResponse(String strIdTransaccion) {
		ServicioMovilLineasResponse response = new ServicioMovilLineasResponse();
		DefaultServiceResponse defaultServiceResponse = new DefaultServiceResponse();
		defaultServiceResponse.setIdRespuesta(PropertiesExternos.CODIGO_EXITO);
		defaultServiceResponse.setMensaje(PropertiesExternos.MENSAJE_EXITO);
		defaultServiceResponse.setExcepcion(null);
		defaultServiceResponse.setIdTransaccional(strIdTransaccion);
		response.setDefaultServiceResponse(defaultServiceResponse);
		return response;
	}
	
	//private BaseException genraBaseException() {
	//	
	//	throw new BaseException(PropertiesExternos.CODIGOERRORTECNICOWS, PropertiesExternos.ERRORTECNICOWS, e);
	//}

	//Detalle de Linea
	@Test
	public void debeInvocarServicioDetalleLineaOk() throws DBException, WSException {		

		numeroDocumento="41643957";
		tipoDocumento="01";
		String numeroMovil="991733567";
		Mockito.when(servicioMovilBusiness.servicioDetalleMovil(Mockito.any(DetalleMovilRequest.class)))
		.thenReturn(Mockito.any(DetalleMovilResponse.class));
		
		servicioEndPoint.consultarDetalleLinea(numeroDocumento,tipoDocumento,numeroMovil);
		Mockito.verify(servicioMovilBusiness).servicioDetalleLinea(Mockito.any(DetalleMovilRequest.class));
	}
	@Test
	public void debeDevolverServicioDetalleLineaBusinessErrorIDF01() throws DBException, WSException {		

		numeroDocumento="";
		tipoDocumento="";
		numeroMovil="";
		Response r=servicioEndPoint.consultarDetalleLinea(numeroDocumento,tipoDocumento,numeroMovil);
		assertEquals("Id respuesta esperada IDF1", PropertiesExternos.CODIGO_ERROR_IDF1, ((DetalleLineaResponse) r.getEntity()).getDefaultServiceResponse().getIdRespuesta());

	}
	//listarMovilCuenta
	@Test
	public void debeInvocarServicioMovilLineasBusinessOk() throws DBException, WSException {		

		numeroDocumento="41643957";
		tipoDocumento="01";
		numeroCuenta="3.190.00.00.100000";
		nivel="01";
		Mockito.when(servicioMovilLineaBusiness.servicioMovilLineas(Mockito.any(ServicioMovilResumenRequest.class)))
		.thenReturn(crearServicioMovilLineasResponse(idTransaccion));
		
		servicioEndPoint.servicioMovilLineas(numeroDocumento,tipoDocumento,numeroCuenta,nivel);
		Mockito.verify(servicioMovilLineaBusiness).servicioMovilLineas(Mockito.any(ServicioMovilResumenRequest.class));
	}
	@Test
	public void debeDevolverServicioMovilLineasBusinessErrorIDF01() throws DBException, WSException {		

		numeroDocumento="";
		tipoDocumento="";
		numeroCuenta="";
		nivel="";
		
		Response r=servicioEndPoint.servicioMovilLineas(numeroDocumento,tipoDocumento,numeroCuenta,nivel);
		assertEquals("Id respuesta esperada IDF1", PropertiesExternos.CODIGO_ERROR_IDF1, ((ServicioMovilLineasResponse) r.getEntity()).getDefaultServiceResponse().getIdRespuesta());

	}	

	//Servicio Movil Resumen
	public void debeInvocarServicioMovilResumenBusinessOk() throws DBException, WSException {		
		ServicioMovilResumenRequest request = getServicioMovilResumenRequest();
		Mockito.when(servicioMovilResumenBusiness.servicioMovilResumen(Mockito.any(ServicioMovilResumenRequest.class)))
		.thenReturn(Mockito.any(ServicioMovilResumenResponse.class));
		

		Mockito.verify(servicioMovilResumenBusiness).servicioMovilResumen(Mockito.any(ServicioMovilResumenRequest.class));
	}
	@Test
	public void debeDevolverErrorIDF01EnServicioMovilResumenBusiness() throws DBException, WSException {		
		ServicioMovilResumenRequest request = getServicioMovilResumenRequest();
		request.setNumeroDocumento("");
		request.setTipoDocumento("");
		
		Response r=servicioEndPoint.servicioMovilResumen(request);
		assertEquals("Id respuesta esperada IDF1", PropertiesExternos.CODIGO_ERROR_IDF1, ((ServicioMovilResumenResponse) r.getEntity()).getDefaultServiceResponse().getIdRespuesta());

	}
		//Registrar linea Favorita
	public void debeInvocarRegistraLineaFavoritaBusinessOk() throws DBException, WSException {		
		RegistraLineaFavoritaRequest request = getRegistraLineaFavoritaRequest();
		Mockito.when(favoritoBusiness.registraLineaFavorita(Mockito.any(RegistraLineaFavoritaRequest.class)))
		.thenReturn(Mockito.any(DefaultServiceResponse.class));
		
		servicioEndPoint.registraLineaFavorita(request);
		Mockito.verify(favoritoBusiness).registraLineaFavorita(Mockito.any(RegistraLineaFavoritaRequest.class));
	}
	@Test
	public void debeDevolverErrorIDF01EnRegistraLineaFavoritaBusiness() throws DBException, WSException {		
		RegistraLineaFavoritaRequest request = getRegistraLineaFavoritaRequest();
		request.setNumeroDocumento("");
		request.setTipoDocumento("");
		request.setNumeroMovil("");
		request.setIdTransaccion("");
		Response r=servicioEndPoint.registraLineaFavorita(request);
		assertEquals("Id respuesta esperada IDF1", PropertiesExternos.CODIGO_ERROR_IDF1, ((RegistraLineaFavoritaResponse) r.getEntity()).getDefaultServiceResponse().getIdRespuesta());

	}
}
