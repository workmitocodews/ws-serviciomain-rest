package app.serviciomain.test.unit;

import app.serviciomain.util.Utilitario;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import app.serviciomain.bean.*;
import app.serviciomain.bussiness.ServicioMovilBusiness;
import app.serviciomain.dao.BscsDao;
import app.serviciomain.dao.CclDao;
import app.serviciomain.exception.DBException;
import app.serviciomain.exception.WSException;
import app.serviciomain.types.*;
import app.serviciomain.util.Constantes;
import app.serviciomain.util.PropertiesExternos;
import app.serviciomain.util.UtilLog;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

//import serviciocorporativo.test.dummy.DummyConsultaAliasCorporativo;
//import serviciocorporativo.test.dummy.DummyConsultaCuentaMoviles;
//import serviciocorporativo.test.dummy.DummyConsultaDetalleMovil;
//import serviciocorporativo.test.dummy.DummyConsultaServicioAlias;
//import pe.com.claro.serviciocorporativo.types.DetalleMovilResponse;

public class ServicioMovilBusinessTest {

	@InjectMocks
	ServicioMovilBusiness service;

	@Mock
	BscsDao bscDao;

	@Mock
	CclDao cclDao;

	@Before
	public void init() {
		service = new ServicioMovilBusiness();
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void debeDevolverListaMoviles() throws WSException, DBException {


		ServicioRequestBase req = new ServicioRequestBase();
		req.setIdTransaccion("111-222-333-444");
		req.setNumeroDocumento("20100053455");
		req.setTipoDocumento("001");

	
			when(bscDao.consultaServiciosMoviles(Mockito.any(ServicioRequestBase.class)))
					.thenReturn(this.dummyConsultaBolsaMoviles(req));

			when(cclDao.consultaAlias(Mockito.any(ServicioRequestBase.class), Mockito.anyInt()))
					.thenReturn(this.dummyConsultaAliasCorporativo(req, 1));



		ServicioMovilResponse response = service.servicioMovil(req);

		assertEquals("Id respuesta esperada Exito", PropertiesExternos.CODIGO_EXITO,
					response.getDefaultServiceResponse().getIdRespuesta());

	}
	@Test
	public void debeDevolverListaMovilesIDF02() throws WSException, DBException {


		ServicioRequestBase req = new ServicioRequestBase();
		req.setIdTransaccion("111-222-333-444");
		req.setNumeroDocumento("20100053455");
		req.setTipoDocumento("001");

		MovilResponse movilResponse= new MovilResponse();
		movilResponse=dummyConsultaBolsaMoviles(req);
		movilResponse.setCodigoRespuesta("1");
			when(bscDao.consultaServiciosMoviles(Mockito.any(ServicioRequestBase.class)))
					.thenReturn(movilResponse);

			when(cclDao.consultaAlias(Mockito.any(ServicioRequestBase.class), Mockito.anyInt()))
					.thenReturn(this.dummyConsultaAliasCorporativo(req, 1));



		ServicioMovilResponse response = service.servicioMovil(req);

		assertEquals("Id respuesta esperada Exito", PropertiesExternos.CODIGO_ERROR_IDF2,
					response.getDefaultServiceResponse().getIdRespuesta());

	}
	@Test
	public void debeDevolverListaDetalleMoviles() throws WSException, DBException {


		DetalleMovilRequest request = new DetalleMovilRequest();
		request.setIdTransaccion("111-222-333-444");
		request.setNumeroDocumento("20100053455");
		request.setTipoDocumento("001");

	
			when(bscDao.consultaDetalleMovil(Mockito.any(DetalleMovilRequest.class)))
					.thenReturn(this.dummyDetalleMovilResponse());

			when(cclDao.consultaServicioAlias(Mockito.any(ServicioAliasRequest.class)))
					.thenReturn(this.dummyConsultaServicioAlias());

			DetalleMovilResponse response = service.servicioDetalleMovil(request);

		assertEquals("Id respuesta esperada Exito", PropertiesExternos.CODIGO_EXITO,
					response.getDefaultServiceResponse().getIdRespuesta());

	}
	@Test(expected = DBException.class)
	public void debeDevolverDBExceptionEnListaDetalleMoviles() throws WSException, DBException {


		DetalleMovilRequest request = new DetalleMovilRequest();
		request.setIdTransaccion("111-222-333-444");
		request.setNumeroDocumento("20100053455");
		request.setTipoDocumento("001");

	
			when(bscDao.consultaDetalleMovil(Mockito.any(DetalleMovilRequest.class))).thenThrow(mockErrorDBExceptionTimeout());

			DetalleMovilResponse response = service.servicioDetalleMovil(request);

		assertEquals("Id respuesta esperada Error", Constantes.CODIGO_ERROR_TIMEOUT,
					response.getDefaultServiceResponse().getIdRespuesta());

	}

	@Test
	public void debeDevolverInformacionDetalleLineaExito() throws WSException, DBException {

		DetalleMovilRequest request = new DetalleMovilRequest();
		request.setIdTransaccion("99db31db-9989-4e54-a1be-b68ac0e3exxx");
		request.setValorServicio("986630717");
		request.setNumeroDocumento("20100053455");
		request.setTipoDocumento("001");

		when(bscDao.consultaDetalleLinea(Mockito.any(DetalleMovilRequest.class)))
				.thenReturn(this.dummyDetalleLineaResponse());

		when(cclDao.consultaServicioAlias(Mockito.any(ServicioAliasRequest.class)))
				.thenReturn(this.dummyConsultaServicioAlias());

		when(bscDao.consultaDetalleMovil(Mockito.any(DetalleMovilRequest.class)))
				.thenReturn(this.dummyDetalleMovilResponse());

		DetalleLineaResponse response = service.servicioDetalleLinea(request);
		UtilLog.printJson(response);
		assertEquals("Id respuesta esperada Exito", PropertiesExternos.CODIGO_EXITO,
				response.getDefaultServiceResponse().getIdRespuesta());

	}
	@Test(expected = DBException.class)
	public void debeDevolverDBExceptionDetalleLineaErro() throws WSException, DBException {


		DetalleMovilRequest request = new DetalleMovilRequest();
		request.setIdTransaccion("99db31db-9989-4e54-a1be-b68ac0e3exxx");
		request.setValorServicio("986630717");
		request.setNumeroDocumento("20100053455");
		request.setTipoDocumento("001");

		when(bscDao.consultaDetalleLinea(Mockito.any(DetalleMovilRequest.class)))
		.thenThrow(mockErrorDBExceptionTimeout());
		
		when(cclDao.consultaServicioAlias(Mockito.any(ServicioAliasRequest.class)))
		.thenThrow(mockErrorDBExceptionTimeout());		
		
			when(bscDao.consultaDetalleMovil(Mockito.any(DetalleMovilRequest.class)))
					.thenThrow(mockErrorDBExceptionTimeout());



			DetalleLineaResponse response = service.servicioDetalleLinea(request);

		assertEquals("Id respuesta esperada Error BD", Constantes.CODIGO_ERROR_TIMEOUT,
					response.getDefaultServiceResponse().getIdRespuesta());

	}
	private ServicioAliasResponse dummyConsultaServicioAlias() {
		ServicioAliasResponse servicioAlias =new ServicioAliasResponse();
	DefaultServiceResponse defaultServiceResponse = new DefaultServiceResponse();
	defaultServiceResponse.setIdRespuesta(PropertiesExternos.CODIGO_EXITO);
	defaultServiceResponse.setMensaje("Exito");
	servicioAlias.setDefaultServiceResponse(defaultServiceResponse);
	servicioAlias.setServicioPrincipal("PO_SERV_PRIN");
	servicioAlias.setValorServicio("PO_SERV_VALOR");
	servicioAlias.setServicioAlias("PO_SERV_ALIAS");
	servicioAlias.setNumeroCuenta("PO_NUMERO_CTA");
	servicioAlias.setDescripcionCuenta("PO_DESCRIPCION_CTA");
	servicioAlias.setCuentaAlias("PO_ALIAS_CTA");
	
	return servicioAlias;
	}
	private DetalleMovilResponse dummyDetalleMovilResponse() {
		DetalleMovilResponse r = new DetalleMovilResponse();
		DefaultServiceResponse defaultServiceResponse = new DefaultServiceResponse();
		defaultServiceResponse.setIdRespuesta(PropertiesExternos.CODIGO_EXITO);
		defaultServiceResponse.setMensaje("Exito");
		r.setEstado("ACTIVO");
		r.setCustomerID("808361");
		r.setCuenta("6.123100.00.00.100000");
		r.setNombrePlan("Bolsa CDI Premium");
		r.setCodigoPlan("67");
		r.setSimCard("8951101339071442004");
		r.setFechaRenovacion("26/03/2014");
		r.setRoaming("ACTIVO");
		r.setNumeroDocumento("20493020618");
		r.setDefaultServiceResponse(defaultServiceResponse);
		return r;
	}
	private DetalleLineaResponse dummyDetalleLineaResponse() {
		DetalleLineaResponse r = new DetalleLineaResponse();
		DefaultServiceResponse defaultServiceResponse = new DefaultServiceResponse();
		defaultServiceResponse.setIdRespuesta(PropertiesExternos.CODIGO_EXITO);
		defaultServiceResponse.setMensaje("Exito");
		r.setDefaultServiceResponse(defaultServiceResponse);
		
		r.setRoaming("SI");
		r.setDescripcionBono("DES_BONO");
		r.setVigenciaBono("VIGENCIA_BONO");
		r.setDescripcionServicioBono("DESCRIPCION_SERVICIO_BONO");
		r.setImei("716101307144200");
		r.setSimCard("8951101339071442004");
		return r;
	}
	private MovilResponse dummyConsultaBolsaMoviles(ServicioRequestBase request) throws DBException {

		MovilResponse r = new MovilResponse();
		r.setCodigoRespuesta(PropertiesExternos.CODIGO_EXITO);
		r.setListaCuentasMoviles(new ArrayList<Movil>());
		
		ArrayList<Movil> lstCursor = new ArrayList<>();
		
			Movil row = new Movil();
			row.setCustomerId(Utilitario.getStringOrEmpty("CUSTOMER_ID"));
			row.setCustcode(Utilitario.getStringOrEmpty("CUSTCODE"));
			row.setLineas(Utilitario.getIntegerOrZero("LINEAS"));
			row.setPlanes(Utilitario.getIntegerOrZero("PLANES"));
			row.setBolsa(Utilitario.getIntegerOrZero("BOLSA"));
			row.setNivel(Utilitario.getIntegerOrZero("NIVEL"));
			row.setCuentaPadre("CUENTA_PADRE");
			row.setCicloFacturacion("CICLOFACTURACION");

			int totalCuentas=2;
			int totalLineas = 20 + row.getLineas();
			int totalBolsas = 3 + row.getBolsa();
			int totalPlanes = 2 + row.getPlanes();

			lstCursor.add(row);
			r.setTotalCuentas(totalCuentas);
			r.setTotalLineas(totalLineas);
			r.setTotalBolsas(totalBolsas);
			r.setTotalPlanes(totalPlanes);
			r.setListaCuentasMoviles(lstCursor);
		return r;
	}

	private AliasResponse dummyConsultaAliasCorporativo(ServicioRequestBase request, int tipoBusqueda) {
		AliasResponse aliasCorporativo = new AliasResponse();
		aliasCorporativo.setCodigoRespuesta(PropertiesExternos.CODIGO_EXITO);
		ArrayList<Alias> lstCursor = new ArrayList<>();

		Alias row = new Alias();

		row.setDescripcion("PO_NUMERO");
		row.setCustcode("PO_DESCRIPCION");
		row.setAlias("PO_ALIAS");
		lstCursor.add(row);

		aliasCorporativo.setListaAliasCorporativo(lstCursor);
		return aliasCorporativo;
	}
	private DBException mockErrorDBExceptionTimeout() {
		return new DBException( Constantes.CODIGO_ERROR_TIMEOUT, "Error de Time Out",new Exception());
	}
	/*
	@Test
	public void debeDevolverDetalleMovil() throws WSException {
		DetalleMovilResponse responseDetalle = new DetalleMovilResponse();
		DefaultServiceResponse defaultServiceResponse = new DefaultServiceResponse();
		defaultServiceResponse.setIdRespuesta("0");
		responseDetalle.setDefaultServiceResponse(defaultServiceResponse);
		
		DetalleMovilRequest requestDetalle = new DetalleMovilRequest();
		ServicioAliasRequest requestAlias = new ServicioAliasRequest();

		requestDetalle.setIdTransaccion("111-222-333-444");
		requestDetalle.setNumeroDocumento("20100053455");
		requestDetalle.setTipoDocumento("001");
		requestDetalle.setValorServicio("956269541");

		try {
			when(bscDao.consultaDetalleMovil(Mockito.any(DetalleMovilRequest.class)))
					.thenReturn(this.dummyConsultaDetalleMovil(requestDetalle));

			when(cclDao.consultaServicioAlias(Mockito.any(ServicioAliasRequest.class)))
					.thenReturn(this.dummyConsultaServicioAlias(requestAlias));

		} catch (DBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		DetalleMovilResponse res;
		try {
			res = service.servicioDetalleMovil(requestDetalle);
			assertTrue("debe Devolver Detalle Movil", res.getCustomerID() != null);
		} catch (DBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


	private DetalleMovilResponse dummyConsultaDetalleMovil(DetalleMovilRequest request) throws DBException {
		DummyConsultaDetalleMovil dummy = new DummyConsultaDetalleMovil();
		return dummy.dummyConsultaDetalleMovil(request.getIdTransaccion());
	}

	private ServicioAliasResponse dummyConsultaServicioAlias(ServicioAliasRequest request)
			throws DBException {
		DummyConsultaServicioAlias dummy = new DummyConsultaServicioAlias();
		return dummy.dummyConsultaServicioAlias(request.getIdTransaccion());
	}
	*/
}
