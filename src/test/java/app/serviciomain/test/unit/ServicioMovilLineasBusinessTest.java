package app.serviciomain.test.unit;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import app.serviciomain.bean.*;
import app.serviciomain.bussiness.ServicioMovilLineasBusiness;
import app.serviciomain.dao.BscsDao;
import app.serviciomain.dao.CclDao;
import app.serviciomain.exception.DBException;
import app.serviciomain.exception.WSException;
import app.serviciomain.types.*;
import app.serviciomain.util.Constantes;
import app.serviciomain.util.PropertiesExternos;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

public class ServicioMovilLineasBusinessTest {
	
	@InjectMocks
	ServicioMovilLineasBusiness service;

	@Mock
	BscsDao bscDao;
	
	@Mock
	CclDao cclDao;

	@Before
	public void init() {
		service = new ServicioMovilLineasBusiness();
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void debeDevolverIDF2() throws WSException, DBException {

		ServicioMovilResumenRequest request = new ServicioMovilResumenRequest();
		request.setIdTransaccion("1234567890");
		request.setTipoDocumento("2"); 
		request.setNumeroDocumento("20143058929");
		request.setNumeroCuenta("7.9249744.00.00.100000");
		
		MovilLineaResponse movilLineaResponse =new MovilLineaResponse();
		movilLineaResponse.setCodigoRespuesta("0");
		movilLineaResponse.setListaMovilLinea(new ArrayList<>());
		
		when(cclDao.listarParametro(Mockito.any(ParametroConsultaRequest.class))).thenReturn(dummyParametroConsultaResponse());
		when(bscDao.consultaServiciosMovilesLineas(Mockito.any(ServicioMovilResumenRequest.class))).thenReturn(movilLineaResponse);

		ServicioMovilLineasResponse response = service.servicioMovilLineas(request);
		assertTrue(PropertiesExternos.CODIGO_ERROR_IDF2.equals(response.getDefaultServiceResponse().getIdRespuesta()));
	}
	
	@Test
	public void debeDevolverListasMovilesOkFavoritoAlias() throws WSException, DBException {

		ServicioMovilResumenRequest request = new ServicioMovilResumenRequest();
		request.setIdTransaccion("1234567890");
		request.setTipoDocumento("001"); 
		request.setNumeroDocumento("20143058929");
		request.setNumeroCuenta("7.9249744.00.00.100000");
		
		MovilLineaResponse movilLineaResponse =new MovilLineaResponse();
		movilLineaResponse.setCodigoRespuesta("0");
		
		ArrayList<MovilLinea> lstCursor = new ArrayList<>();
		MovilLinea rowLinea = new MovilLinea();		
		rowLinea.setBloqueSuspendido("er"); 
		rowLinea.setBolsa("ee");
		rowLinea.setContrato(1);
		rowLinea.setEstadoContrato("a");
		rowLinea.setLinea("975161105");
		rowLinea.setOfertaProducto("er"); //ANTES PO
		rowLinea.setTipoPlan("");
		
		lstCursor.add(rowLinea);

		movilLineaResponse.setListaMovilLinea(lstCursor);
		movilLineaResponse.setMensajeRespuesta("exito");
		
		AliasLineaResponse aliasResponse = new AliasLineaResponse();
		aliasResponse.setCodigoRespuesta("0");
		aliasResponse.setMensajeRespuesta("exito");
		List<AliasLinea> listaAlias = new ArrayList<>();
		AliasLinea rowAlias = new AliasLinea();
		
		rowAlias.setAliasAsociado("AliasTest");
		rowAlias.setCodCuenta("cuenta");
		rowAlias.setCodRecibo("recibo");
		rowAlias.setLineaMovil("51975161105");
		
		listaAlias.add(rowAlias);
		aliasResponse.setListaAliasLineaCorporativo(listaAlias);
		
		FavoritaLineaResponse favoritoResponse = new FavoritaLineaResponse();
		ArrayList<FavoritaLinea> listaFavorito = new ArrayList<>();
		favoritoResponse.setCodigoRespuesta("0");
		favoritoResponse.setMensajeRespuesta("exito");
		
		FavoritaLinea rowFav = new FavoritaLinea();
		
		rowFav.setCuenta("sdf"); 		
		rowFav.setSubCuenta("sdfdf"); 
		rowFav.setLinea("975161105"); 
		
		listaFavorito.add(rowFav);
		favoritoResponse.setListaFavoritaLineaCorporativo(listaFavorito);
		
		when(cclDao.listarParametro(Mockito.any(ParametroConsultaRequest.class))).thenReturn(dummyParametroConsultaResponse());
		when(bscDao.consultaServiciosMovilesLineas(Mockito.any(ServicioMovilResumenRequest.class))).thenReturn(movilLineaResponse);
		when(cclDao.consultaObtenerAlias(request)).thenReturn(aliasResponse);
		when(bscDao.consultaObtenerFavorito(request)).thenReturn(favoritoResponse);
		

		
		ServicioMovilLineasResponse response = service.servicioMovilLineas(request);
		
		assertTrue(PropertiesExternos.CODIGO_EXITO.equals(response.getDefaultServiceResponse().getIdRespuesta()));
		assertTrue(response.getListaLineasMoviles().get(0).getFavorita().equalsIgnoreCase("1"));
		assertTrue(response.getListaLineasMoviles().get(0).getAlias().equalsIgnoreCase("AliasTest"));
	}
	
	@Test
	public void debeDevolverListasMovilesOkSinFavoritoSinAlias() throws WSException, DBException {

		ServicioMovilResumenRequest request = new ServicioMovilResumenRequest();
		request.setIdTransaccion("1234567890");
		request.setTipoDocumento("001"); 
		request.setNumeroDocumento("20143058929");
		request.setNumeroCuenta("7.9249744.00.00.100000");
		
		//--- MovilLineaResponse
		MovilLineaResponse movilLineaResponse =new MovilLineaResponse();
		movilLineaResponse.setCodigoRespuesta(Constantes.CADENA_CERO);
		
		ArrayList<MovilLinea> lstCursor = new ArrayList<>();
		MovilLinea rowLinea = new MovilLinea();		
		rowLinea.setBloqueSuspendido("er"); 
		rowLinea.setBolsa("ee");
		rowLinea.setContrato(1);
		rowLinea.setEstadoContrato("a");
		rowLinea.setLinea("975161105");
		rowLinea.setOfertaProducto("er"); //ANTES PO
		rowLinea.setTipoPlan("");
		
		lstCursor.add(rowLinea);

		movilLineaResponse.setListaMovilLinea(lstCursor);
		movilLineaResponse.setMensajeRespuesta("exito");
		
		AliasLineaResponse aliasResponse = new AliasLineaResponse();
		aliasResponse.setCodigoRespuesta(Constantes.CADENA_CERO);
		aliasResponse.setMensajeRespuesta("exito");
		
		List<AliasLinea> listaAlias = new ArrayList<>();
/*		AliasLinea rowAlias = new AliasLinea();		
		rowAlias.setAliasAsociado("AliasTest");
		rowAlias.setCodCuenta("cuenta");
		rowAlias.setCodRecibo("recibo");
		rowAlias.setLineaMovil("975432123");		
		listaAlias.add(rowAlias);*/
		aliasResponse.setListaAliasLineaCorporativo(listaAlias);
		
		FavoritaLineaResponse favoritoResponse = new FavoritaLineaResponse();
		ArrayList<FavoritaLinea> listaFavorito = new ArrayList<>();
		favoritoResponse.setCodigoRespuesta(Constantes.CADENA_CERO);
		favoritoResponse.setMensajeRespuesta("exito");		
/*		FavoritaLinea rowFav = new FavoritaLinea();		
		rowFav.setCuenta("sdf"); 		
		rowFav.setSubCuenta("sdfdf"); 
		rowFav.setLinea("975432123"); 		
		listaFavorito.add(rowFav);*/
		
		favoritoResponse.setListaFavoritaLineaCorporativo(listaFavorito);
		when(cclDao.listarParametro(Mockito.any(ParametroConsultaRequest.class))).thenReturn(dummyParametroConsultaResponse());
		when(bscDao.consultaServiciosMovilesLineas(Mockito.any(ServicioMovilResumenRequest.class))).thenReturn(movilLineaResponse);
		when(bscDao.consultaObtenerFavorito(request)).thenReturn(favoritoResponse);
		when(cclDao.consultaObtenerAlias(request)).thenReturn(aliasResponse);
		
		ServicioMovilLineasResponse response = service.servicioMovilLineas(request);
		assertEquals("Id respuesta esperada Exito", PropertiesExternos.CODIGO_EXITO, response.getDefaultServiceResponse().getIdRespuesta());

	}
	
	
	@Test
	public void debeDevolverListasMovilesOkSuspendida() throws WSException, DBException {

		ServicioMovilResumenRequest request = new ServicioMovilResumenRequest();
		request.setIdTransaccion("1234567890");
		request.setTipoDocumento("001"); 
		request.setNumeroDocumento("20143058929");
		request.setNumeroCuenta("7.9249744.00.00.100000");
		
		MovilLineaResponse movilLineaResponse =new MovilLineaResponse();
		movilLineaResponse.setCodigoRespuesta("0");
		
		ArrayList<MovilLinea> lstCursor = new ArrayList<>();
		MovilLinea rowLinea = new MovilLinea();		
		rowLinea.setBloqueSuspendido("er"); 
		rowLinea.setBolsa("ee");
		rowLinea.setContrato(1);
		rowLinea.setEstadoContrato("a");
		rowLinea.setLinea("123123");
		rowLinea.setOfertaProducto("er"); //ANTES PO
		rowLinea.setTipoPlan("");
		lstCursor.add(rowLinea);
		
		movilLineaResponse.setListaMovilLinea(lstCursor);
		movilLineaResponse.setMensajeRespuesta("exito");
		
		AliasLineaResponse aliasResponse = new AliasLineaResponse();
		aliasResponse.setCodigoRespuesta("0");
		aliasResponse.setMensajeRespuesta("exito");
		List<AliasLinea> listaAlias = new ArrayList<>();
		AliasLinea rowAlias = new AliasLinea();
		
		rowAlias.setAliasAsociado("aa");
		rowAlias.setCodCuenta("cuenta");
		rowAlias.setCodRecibo("recibo");
		rowAlias.setLineaMovil("123123");
		
		listaAlias.add(rowAlias);
		
		
		FavoritaLineaResponse favoritoResponse = new FavoritaLineaResponse();
		ArrayList<FavoritaLinea> listaFavorito = new ArrayList<>();
		favoritoResponse.setCodigoRespuesta("0");
		favoritoResponse.setMensajeRespuesta("exito");
		
		FavoritaLinea rowFav = new FavoritaLinea();
		
		rowFav.setCuenta("sdf"); 		
		rowFav.setSubCuenta("sdfdf"); 
		rowFav.setLinea("00123123"); 
		
		listaFavorito.add(rowFav);
		favoritoResponse.setListaFavoritaLineaCorporativo(listaFavorito);
		
		when(cclDao.listarParametro(Mockito.any(ParametroConsultaRequest.class))).thenReturn(dummyParametroConsultaResponse());
		when(bscDao.consultaServiciosMovilesLineas(Mockito.any(ServicioMovilResumenRequest.class))).thenReturn(movilLineaResponse);
		when(cclDao.consultaObtenerAlias(request)).thenReturn(aliasResponse);
		when(bscDao.consultaObtenerFavorito(request)).thenReturn(favoritoResponse);
		
		aliasResponse.setListaAliasLineaCorporativo(listaAlias);
		
		ServicioMovilLineasResponse response = service.servicioMovilLineas(request);
		
		assertTrue(PropertiesExternos.CODIGO_EXITO.equals(response.getDefaultServiceResponse().getIdRespuesta()));
	}
	/*@Test
	public void debeHomologaTipoDocumento() throws DBException {

		ServicioMovilResumenRequest request = new ServicioMovilResumenRequest();
		request.setIdTransaccion("1234567890");
		request.setTipoDocumento("001");
			
		when(cclDao.listarParametro(Mockito.any(ParametroConsultaRequest.class))).thenReturn(dummyParametroConsultaResponse());
		
		String tipoDocumento = service.homologaTipoDocumento(PropertiesExternos.DBBSCS_NOMBRE, request.getTipoDocumento(), request.getIdTransaccion());

		assertEquals("6", tipoDocumento);
	}*/
	
	private ParametroConsultaResponse dummyParametroConsultaResponse() {
		
		ParametroConsultaResponse parametroConsultaResponse = new ParametroConsultaResponse();		
		
		parametroConsultaResponse.setIdRespuesta(PropertiesExternos.CODIGO_EXITO);
		parametroConsultaResponse.setMensaje("PROCESO EXITOSO");
		
		ArrayList<Parametro> lstCursor = new ArrayList<>();
		Parametro parametro = new Parametro();
		
		//DNI
		parametro.setPadre("BSCS_HOMOLOGA_TIPO_DOCUMENTO");
		parametro.setCodigo("BSCS_002");
		parametro.setDescripcion("DNI");
		parametro.setValor("2");
		parametro.setEstado("A");
		
		lstCursor.add(parametro);
		
		//RUC
		parametro.setPadre("BSCS_HOMOLOGA_TIPO_DOCUMENTO");
		parametro.setCodigo("BSCS_001");
		parametro.setDescripcion("RUC");
		parametro.setValor("6");
		parametro.setEstado("A");
		
		lstCursor.add(parametro);
				
		parametroConsultaResponse.setListaParametros(lstCursor);
		
		return parametroConsultaResponse;
	}

	/*@Test
	public void debeInvocarListarParametro() throws DBException, WSException {

		ServicioRequestBase request = crearDatosFavoritoRequest();
		String codigoPadre = PropertiesExternos.DBBSCS_NOMBRE + Constantes.GUIONBAJO + PropertiesExternos.STR_HOMOLOGA_TIPO_DOCUMENTO;
		ParametroConsultaRequest requestParam = new ParametroConsultaRequest();
		requestParam.setCodigoPadre(codigoPadre);
		requestParam.setIdTransaccion(request.getIdTransaccion());
		
		when(cclDao.listarParametro(Mockito.any(ParametroConsultaRequest.class))).thenReturn(this.dummyConsultaParametro(requestParam));
		
		String tipoDocumento = service.homologaTipoDocumento(PropertiesExternos.DBBSCS_NOMBRE, request.getTipoDocumento(), request.getIdTransaccion());

		assertEquals("2", tipoDocumento);
	}*/
	
	private ServicioRequestBase crearDatosFavoritoRequest() {
		
		ServicioRequestBase request= new ServicioRequestBase();
		request.setNumeroDocumento("20100053455");
		request.setTipoDocumento("001");
		request.setIdTransaccion("2361253621536256399");

		return request;
	}
	
	public ParametroConsultaResponse dummyConsultaParametro(ParametroConsultaRequest requestParam) throws DBException {
		
		ParametroConsultaResponse response = new ParametroConsultaResponse();
	
		response.setIdRespuesta(PropertiesExternos.CODIGO_EXITO);
		response.setMensaje("PROCESO EXITOSO");
		ArrayList<Parametro> lstCursor = new ArrayList<>();
		Parametro row = new Parametro();
		
		//DNI
		row.setPadre("BSCS_HOMOLOGA_TIPO_DOCUMENTO");
		row.setCodigo("BSCS_002");
		row.setDescripcion("DNI");
		row.setValor("6");
		row.setEstado("A");
		
		lstCursor.add(row);
		
		//RUC
		row.setPadre("BSCS_HOMOLOGA_TIPO_DOCUMENTO");
		row.setCodigo("BSCS_001");
		row.setDescripcion("Tipo Documento Factura  equivalente en BSCS");
		row.setValor("2");
		row.setEstado("A");
		
		lstCursor.add(row);
		
		response.setListaParametros(lstCursor);
			
		return response;
	}

}
