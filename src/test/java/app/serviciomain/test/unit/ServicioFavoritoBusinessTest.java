package app.serviciomain.test.unit;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import app.serviciomain.bean.DefaultServiceResponse;
import app.serviciomain.bean.Favorito;
import app.serviciomain.bean.Parametro;
import app.serviciomain.bussiness.FavoritoBusiness;
import app.serviciomain.dao.BscsDao;
import app.serviciomain.dao.CclDao;
import app.serviciomain.exception.DBException;
import app.serviciomain.exception.WSException;
import app.serviciomain.types.*;
import app.serviciomain.util.Constantes;
import app.serviciomain.util.PropertiesExternos;
import app.serviciomain.util.UtilLog;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

public class ServicioFavoritoBusinessTest {
	@InjectMocks
	public FavoritoBusiness business;
	
	@Mock
	BscsDao bscsDao;
	@Mock
	CclDao cclDao;
	@Mock
	private UtilLog utilLog;
	
	@Before
	public void init() {

		business=new FavoritoBusiness();
		MockitoAnnotations.initMocks(this);		
	}
	

	@Test
	public void debeResponderErrorFuncional() throws WSException {

		ServicioRequestBase request= new ServicioRequestBase();		
		ConsultaFavoritoResponse r = business.listaFavorito(request);		
		assertEquals(PropertiesExternos.CODIGO_ERROR_IDF1,r.getIdRespuesta());
		
	}

	private ServicioRequestBase crearDatosFavoritoRequest() {
		
		ServicioRequestBase request= new ServicioRequestBase();
		request.setNumeroDocumento("20100053455");
		request.setTipoDocumento("001");
		request.setIdTransaccion("2361253621536256399");

		return request;
	}
	private RegistraLineaFavoritaRequest crearDatosRegistraFavoritoRequest() {
		
		RegistraLineaFavoritaRequest request= new RegistraLineaFavoritaRequest();
		request.setNumeroDocumento("20100053455");
		request.setTipoDocumento("001");
		request.setNumeroCuenta("9.000.001.00009");
		request.setNumeroMovil("999788787");
		request.setIdTransaccion("2361253621536256399");

		return request;
	}
	public ServicioAliasResponse dummyConsultaServicioAlias(final String idTransaccion) throws DBException {
		
		ServicioAliasResponse response = new ServicioAliasResponse();
		DefaultServiceResponse defaultService = new DefaultServiceResponse(); 

		defaultService.setIdRespuesta("0");
		defaultService.setMensaje("PROCESO EXITOSO");
		
		response.setDefaultServiceResponse(defaultService);

		response.setCuentaAlias("AliasRenzo6tyu");
		response.setDescripcionCuenta("6.150964.00.00.100000");
		response.setServicioAlias("AliasNumCel");
			
		return response;
	}
	public ParametroConsultaResponse dummyConsultaParametro(ParametroConsultaRequest requestParam) throws DBException {
		
		ParametroConsultaResponse response = new ParametroConsultaResponse();
		//DefaultServiceResponse defaultService = new DefaultServiceResponse(); 

		response.setIdRespuesta(PropertiesExternos.CODIGO_EXITO);
		response.setMensaje("PROCESO EXITOSO");
		ArrayList<Parametro> lstCursor = new ArrayList<>();
		Parametro row = new Parametro();
		row.setPadre("BSCS_HOMOLOGA_TIPO_DOCUMENTO");
		row.setCodigo("BSCS_001");
		row.setDescripcion("Tipo Documento Factura  equivalente en BSCS");
		row.setValor("6");
		row.setEstado("A");

		lstCursor.add(row);
		
		response.setListaParametros(lstCursor);
			
		return response;
	}
	public ConsultaFavoritoResponse dummyConsultaFavoritos(ServicioRequestBase request) throws DBException {
		
		ConsultaFavoritoResponse response = new ConsultaFavoritoResponse();
		//DefaultServiceResponse defaultService = new DefaultServiceResponse(); 

		response.setIdRespuesta(PropertiesExternos.CODIGO_EXITO);
		response.setMensaje("PROCESO EXITOSO");
		
		ArrayList<Favorito> lstCursor = new ArrayList<>();
		Favorito row = new Favorito();
		row.setCuenta("6.150956");
		row.setLinea("940494697");
		row.setSubcuenta("966995");
		row.setAlias("");


		lstCursor.add(row);
		
		response.setListaFavoritos(lstCursor);
			
		return response;
	}
	
public DefaultServiceResponse dummyRegistraFavorito(RegistraLineaFavoritaRequest request) throws DBException {
		
    	DefaultServiceResponse response = new DefaultServiceResponse();

		response.setIdRespuesta(PropertiesExternos.CODIGO_EXITO);
		response.setMensaje("PROCESO EXITOSO");
				
		return response;
	}
}
