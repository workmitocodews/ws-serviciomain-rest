package app.serviciomain.test.unit;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import app.serviciomain.bussiness.FavoritoBusiness;
import app.serviciomain.exception.WSException;
import app.serviciomain.rest.Servicio;
import app.serviciomain.types.ConsultaFavoritoResponse;
import app.serviciomain.types.ServicioRequestBase;
import app.serviciomain.util.PropertiesExternos;
import app.serviciomain.util.UtilLog;

import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;

import static org.junit.Assert.assertEquals;

public class ServicioFavoritoEndPointTest {
	@InjectMocks
	Servicio favoritoEndpoint;
	
	@Mock
	private FavoritoBusiness consultaBusiness;
	@Mock
	private UtilLog utilLog;
	@Mock
	HttpHeaders httpHeaders;
	
	@Before
	public void init() {
		favoritoEndpoint=new Servicio();
		MockitoAnnotations.initMocks(this);			
	}
	
	@Test
	public void debeInvocarAlBussines() throws WSException {
		String nrodocumento="80068607007";
		String tipodocumento="001";
		getHttpHeader();
		favoritoEndpoint.listaFavorito(nrodocumento,tipodocumento);
		Mockito.verify(consultaBusiness).listaFavorito(Mockito.any(ServicioRequestBase.class));		
	}
	
	@Test
	public void debeInvocarAlBussinesFail() throws WSException {
		String nrodocumento="80068607007";
		String tipodocumento="001";
		getHttpHeader();
		Mockito.when(consultaBusiness.listaFavorito(Mockito.any(ServicioRequestBase.class)))
		.thenThrow(new WSException(PropertiesExternos.CODIGOERRORTECNICOWS, PropertiesExternos.ERRORTECNICOWS, new Exception()));
		Response r=favoritoEndpoint.listaFavorito(nrodocumento,tipodocumento);
		assertEquals("Id respuesta esperada IDF1", PropertiesExternos.CODIGOERRORTECNICOWS, ((ConsultaFavoritoResponse) r.getEntity()).getIdRespuesta());

	}
	
	
	public void getHttpHeader() {		
		httpHeaders = Mockito.mock(HttpHeaders.class);		
		Mockito.when(httpHeaders.getHeaderString("idTransaccion")).thenReturn("236125362153625632");	
	}
}
