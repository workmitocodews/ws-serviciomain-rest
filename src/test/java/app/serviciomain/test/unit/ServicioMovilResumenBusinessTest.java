package app.serviciomain.test.unit;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import app.serviciomain.bean.Movil;
import app.serviciomain.bean.MovilResponse;
import app.serviciomain.bean.MovilResumenResponse;
import app.serviciomain.bussiness.ServicioMovilResumenBusiness;
import app.serviciomain.dao.BscsDao;
import app.serviciomain.exception.DBException;
import app.serviciomain.exception.WSException;
import app.serviciomain.types.ServicioMovilResumenRequest;
import app.serviciomain.types.ServicioMovilResumenResponse;
import dummy.DummyConsultaBolsasMoviles;

import java.util.ArrayList;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

public class ServicioMovilResumenBusinessTest {

	@InjectMocks
	ServicioMovilResumenBusiness service;

	@Mock
	BscsDao bscDao;

	@Before
	public void init() {
		service = new ServicioMovilResumenBusiness();
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void debeDevolverServicioMovilResumen() throws WSException {
		MovilResponse r = new MovilResponse();
		r.setCodigoRespuesta("0");
		r.setListaCuentasMoviles(new ArrayList<Movil>());

		ServicioMovilResumenRequest request = new ServicioMovilResumenRequest();
		request.setTipoDocumento("001");
		request.setNumeroDocumento("20100053455");
		request.setNumeroCuenta("6.150950.00.00.100004");

		try {
			when(bscDao.consultaServiciosMovilesResumen(Mockito.any(ServicioMovilResumenRequest.class)))
					.thenReturn(this.dummyMovilResumen(request));

		} catch (DBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		ServicioMovilResumenResponse res;
		try {
			res = service.servicioMovilResumen(request);
			assertTrue("debe Devolver Listado", res.getListaResumenMoviles() != null);
		} catch (DBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private MovilResumenResponse dummyMovilResumen(ServicioMovilResumenRequest request) throws DBException {
		DummyConsultaBolsasMoviles dummy = new DummyConsultaBolsasMoviles();
		return dummy.dummyConsultaBolsaMoviles(request.getIdTransaccion());
	}

}
