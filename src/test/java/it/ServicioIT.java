package it;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.container.test.api.RunAsClient;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.test.api.ArquillianResource;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import app.serviciomain.types.*;
import app.serviciomain.util.Constantes;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import static org.junit.Assert.assertEquals;

/***
  *
 *         Clase Test de Integracion basica. Note que esta clase Termina en IT
 *         (integration test), esto es para ser administrada por el plugin de
 *         maven como tal.
 */

@RunWith(Arquillian.class)
public class ServicioIT {

	private static final Logger logger = LogManager.getLogger(ServicioIT.class);
	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ", Locale.getDefault());

	@Deployment(testable = true)
	public static WebArchive createDeployment() {
		return ShrinkWrap.create(WebArchive.class, "ServicioIT.war")
				.addPackages(true, "pe.com.claro.serviciocorporativo")
				.addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml");
	}

	@BeforeClass
	public static void setupClass() {
		logger.info("Preparacion inicial para las pruebas");
		System.out.println("Preparacion inicial para las pruebas");

	}

	@AfterClass
	public static void tearDownClass() {
		logger.info("Liberacion de recursos usados en las pruebas");
		System.out.println("Liberacion de recursos usados en las pruebas");
	}

	@Test
	@RunAsClient
	public void testServicioMovil(@ArquillianResource URL url) {
		// arrange
		Client cli = ClientBuilder.newClient();
		String urlTargetServicioMovil = url + "resources/servicios/movil";

		WebTarget targetServicioMovil = cli.target(urlTargetServicioMovil);

		ServicioRequestBase request = new ServicioRequestBase();
		request.setTipoDocumento("001");
		request.setNumeroDocumento("20100053455");

		Response response = targetServicioMovil.request(MediaType.APPLICATION_JSON).headers(getHttpHeaders())
				.post(Entity.entity(request, MediaType.APPLICATION_JSON));

		logger.info("mensaje " + response.getEntity().toString());

		// assert
		ServicioMovilResponse res = response.readEntity(ServicioMovilResponse.class);
		System.out.println("testServicioMovil: " + res.getDefaultServiceResponse().getMensaje());

		assertEquals("Respuesta debe ser exitosa", "0", res.getDefaultServiceResponse().getIdRespuesta());

	}
	
	/*@Test
	@RunAsClient
	public void testServicioFijo(@ArquillianResource URL url) {
		// arrange
		Client cli = ClientBuilder.newClient();
		String urlTargetServicioFijo = url + "resources/servicios/fijo";

		WebTarget targetServicioFijo = cli.target(urlTargetServicioFijo);

		ServicioRequestBase request = new ServicioRequestBase();
		request.setTipoDocumento("001");
		request.setNumeroDocumento("20100053455");

		Response response = targetServicioFijo.request(MediaType.APPLICATION_JSON).headers(getHttpHeaders())
				.post(Entity.entity(request, MediaType.APPLICATION_JSON));

		logger.info("mensaje " + response.getEntity().toString());

		// assert
		ServicioFijoResponse res = response.readEntity(ServicioFijoResponse.class);
		System.out.println("testServicioFijo: "+ res.getDefaultServiceResponse().getMensaje());

		assertEquals("Respuesta debe ser exitosa", "0", res.getDefaultServiceResponse().getIdRespuesta());
	}*/
	

	@Test
	@RunAsClient
	public void testServicioMovilIDF1(@ArquillianResource URL url) {
		Client cli = ClientBuilder.newClient();
		String urlTargetServicioMovil = url + "resources/servicios/movil";

		WebTarget targetServicioMovil = cli.target(urlTargetServicioMovil);

		ServicioRequestBase request = new ServicioRequestBase();
		request.setTipoDocumento("001");
		request.setNumeroDocumento("");

		Response response = targetServicioMovil.request(MediaType.APPLICATION_JSON).headers(getHttpHeaders())
				.post(Entity.entity(request, MediaType.APPLICATION_JSON));

		ServicioMovilResponse res = response.readEntity(ServicioMovilResponse.class);

		assertEquals("Respuesta debe ser exitosa IDF1", "1", res.getDefaultServiceResponse().getIdRespuesta());
	}
	

	@Test
	@RunAsClient
	public void testConsultaDetalleMovilOK(@ArquillianResource URL url) throws JsonProcessingException, IOException {
		// arrange
		System.out.println("Ejecuta testConsultaDetalleMovilOK()"+url);
		Client cli = ClientBuilder.newClient();
		String urlCliente = url + "resources/servicios/movil/detalle?tipoDocumento=001&numeroDocumento=20100053455&numeroMovil=999999999";
		System.out.println("Ejecuta urlCliente"+urlCliente);
		WebTarget target = cli.target(urlCliente);
		Response response = target.request(MediaType.APPLICATION_JSON).headers(getHttpHeaders()).get();
		String entStr=response.readEntity((String.class));
		ObjectMapper mapper=new ObjectMapper();
		JsonNode obj=mapper.readTree(entStr);
		assertEquals("0",obj.get("defaultServiceResponse").get("idRespuesta").asText());
	}
	
	private MultivaluedMap<String, Object> getHttpHeaders() {
		MultivaluedMap<String, Object> map = new MultivaluedHashMap<String, Object>();
		map.add(Constantes.IDTRANSACCION, "abcd-1234");
		map.add(Constantes.MSGID, "1234");
		map.add(Constantes.USERID, "userTest");
		map.add(Constantes.ACCEPT, MediaType.APPLICATION_JSON);
		map.add(Constantes.TIMESTAMP, sdf.format(new Date()));
		return map;
	}

}
