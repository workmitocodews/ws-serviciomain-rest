package it;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.container.test.api.RunAsClient;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.test.api.ArquillianResource;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import app.serviciomain.types.*;
import app.serviciomain.util.Constantes;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import static org.junit.Assert.assertEquals;

/***
 *
 *         Clase Test de Integracion basica. Note que esta clase Termina en IT
 *         (integration test), esto es para ser administrada por el plugin de
 *         maven como tal.
 */

@RunWith(Arquillian.class)
public class ResumenIT {

	private static final Logger logger = LogManager.getLogger(ResumenIT.class);
	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ", Locale.getDefault());

	@Deployment(testable = true)
	public static WebArchive createDeployment() {
		return ShrinkWrap.create(WebArchive.class, "ResumenIT.war")
				.addPackages(true, "pe.com.claro.serviciocorporativo")
				.addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml");
	}

	@BeforeClass
	public static void setupClass() {
		logger.info("Preparacion inicial para las pruebas");
		System.out.println("Preparacion inicial para las pruebas");

	}

	@AfterClass
	public static void tearDownClass() {
		logger.info("Liberacion de recursos usados en las pruebas");
		System.out.println("Liberacion de recursos usados en las pruebas");
	}

	@Test
	@RunAsClient
	public void testServicioMovilResumen(@ArquillianResource URL url) {
		// arrange
		Client cli = ClientBuilder.newClient();
		String urlTargetServicioMovilResumen = url + "resources/servicios/movil/resumen";

		WebTarget targetServicioMovilResumen = cli.target(urlTargetServicioMovilResumen);

		ServicioMovilResumenRequest request = new ServicioMovilResumenRequest();
		request.setTipoDocumento("001");
		request.setNumeroDocumento("20143058929");
		request.setNumeroCuenta("7.9249744.00.00.100000");

		Response response = targetServicioMovilResumen.request(MediaType.APPLICATION_JSON).headers(getHttpHeaders())
				.post(Entity.entity(request, MediaType.APPLICATION_JSON));

		logger.info("mensaje " + response.getEntity().toString());

		// assert
		ServicioMovilResumenResponse res = response.readEntity(ServicioMovilResumenResponse.class);
		System.out.println("testServicioMovilResumen: " + res.getDefaultServiceResponse().getMensaje());

		assertEquals("Respuesta debe ser exitosa", "0", res.getDefaultServiceResponse().getIdRespuesta());

	}


	@Test
	@RunAsClient
	public void testServicioMovilResumenIDF1(@ArquillianResource URL url) {
		Client cli = ClientBuilder.newClient();
		String urlTargetServicioMovilResumen = url + "resources/servicios/movil/resumen";

		WebTarget targetServicioMovilResumen = cli.target(urlTargetServicioMovilResumen);

		ServicioMovilResumenRequest request = new ServicioMovilResumenRequest();
		request.setTipoDocumento("001");
		request.setNumeroDocumento("");
		request.setNumeroCuenta("7.9249744.00.00.100000");

		Response response = targetServicioMovilResumen.request(MediaType.APPLICATION_JSON).headers(getHttpHeaders())
				.post(Entity.entity(request, MediaType.APPLICATION_JSON));

		ServicioMovilResumenResponse res = response.readEntity(ServicioMovilResumenResponse.class);
		assertEquals("Respuesta debe ser exitosa IDF1", "1", res.getDefaultServiceResponse().getIdRespuesta());
	}



	private MultivaluedMap<String, Object> getHttpHeaders() {
		MultivaluedMap<String, Object> map = new MultivaluedHashMap<String, Object>();
		map.add(Constantes.IDTRANSACCION, "abcd-1234");
		map.add(Constantes.MSGID, "1234");
		map.add(Constantes.USERID, "userTest");
		map.add(Constantes.ACCEPT, MediaType.APPLICATION_JSON);
		map.add(Constantes.TIMESTAMP, sdf.format(new Date()));
		return map;
	}

}
