package it;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.container.test.api.RunAsClient;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.test.api.ArquillianResource;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Test;
import org.junit.runner.RunWith;
import app.serviciomain.util.Constantes;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import static org.junit.Assert.assertEquals;

@RunWith(Arquillian.class)
public class FavoritoIT {
	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ", Locale.getDefault());

	@Deployment(testable = true)
	public static WebArchive createDeployment() {
		return ShrinkWrap.create(WebArchive.class, "FavoritoIT.war")
				.addPackages(true, "pe.com.claro.serviciocorporativo")
				.addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml");
	}
	
	@Test
	@RunAsClient
	public void testConsultaFavoritosOK(@ArquillianResource URL url) throws JsonParseException, JsonMappingException, IOException {

		Client cli = ClientBuilder.newClient();
		System.out.println(url);
		String urlCliente = url + "resources/servicios/favorito/lista?tipoDocumento=001&numeroDocumento=20100053455";
		System.out.println(urlCliente);
		WebTarget target = cli.target(urlCliente);
		Response response = target.request(MediaType.APPLICATION_JSON).headers(getHttpHeaders()).get();
		String entStr=response.readEntity((String.class));
		ObjectMapper mapper=new ObjectMapper();
		JsonNode obj=mapper.readTree(entStr);
				
		assertEquals("0",obj.get("idRespuesta").asText());
	}
	private MultivaluedMap<String, Object> getHttpHeaders() {
		MultivaluedMap<String, Object> map = new MultivaluedHashMap<String, Object>();
		map.add(Constantes.IDTRANSACCION, "abcd-1234");
		map.add(Constantes.MSGID, "1234");
		map.add(Constantes.USERID, "userTest");
		map.add(Constantes.ACCEPT, MediaType.APPLICATION_JSON);
		map.add(Constantes.TIMESTAMP, sdf.format(new Date()));
		return map;
	}
	
}
