package app.serviciomain.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import app.serviciomain.exception.GeneralRuntimeException;

public class PropertiesManager {

	private PropertiesManager() {
	}

	public static Map<String, Object> getProperties() {
		String nombrePropertieExterno = ".properties";
		Map<String, Object> dataProperties = new HashMap<>();
		dataProperties.putAll(readProperties(nombrePropertieExterno, false));
		return dataProperties;
	}

	private static Map<String, Object> readProperties(String fileInClasspath, Boolean interno) {
		String urlServer;
		urlServer = System.getProperty("claro.properties") + Constantes.NOMBRE_API + File.separator + fileInClasspath;

		try (InputStream is = interno ? PropertiesManager.class.getClassLoader().getResourceAsStream(fileInClasspath)
				: new FileInputStream(urlServer);) {

			Map<String, Object> map = new HashMap<>();
			Properties properties = new Properties();
			properties.load(is);

			for (Map.Entry<Object, Object> entry : properties.entrySet()) {
				map.put(entry.getKey().toString(), entry.getValue());
			}

			return map;

		} catch (Exception e) {

			throw new GeneralRuntimeException("No se puede leer el archivo " + fileInClasspath + " - " + urlServer, e);

		}
	}
}
