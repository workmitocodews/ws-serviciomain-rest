package app.serviciomain.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.w3c.dom.Node;
import org.w3c.dom.bootstrap.DOMImplementationRegistry;
import org.w3c.dom.ls.DOMImplementationLS;
import org.w3c.dom.ls.LSSerializer;
import org.xml.sax.InputSource;
import app.serviciomain.dao.CclDao;
import app.serviciomain.exception.BaseException;

import javax.inject.Inject;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.Marshaller;
import javax.xml.namespace.QName;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.StringReader;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class Utilitario {

	@Inject
	public CclDao cclDao;

	public static final UtilLog utilLog = new UtilLog();

	@SuppressWarnings("rawtypes")
	private static HashMap<Class, JAXBContext> mapContexts = new HashMap<Class, JAXBContext>();

	private Utilitario() {

	}

	private static Logger logger = LogManager.getLogger(app.serviciomain.util.Utilitario.class);
	private static Validator validator = Validation.buildDefaultValidatorFactory().getValidator();

	public static String convertProperties(Object object) {
		String a = null;
		if (object != null) {
			a = object.toString();
			try {
				a = new String(a.getBytes(StandardCharsets.ISO_8859_1), StandardCharsets.UTF_8);
			} catch (Exception e) {
				logger.error(e);
			}
		}
		return a;
	}

	public static Calendar toCalendar(final String iso8601string) {
		Calendar calendar = GregorianCalendar.getInstance();
		try {
			boolean exito = false;
			String s = iso8601string.replace("Z", "+00:00");
			if (iso8601string.length() == 20) { // *** Sin Precision de Milisegundos
				s = s.substring(0, 22) + s.substring(23);
				Date date = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ", Locale.getDefault()).parse(s);
				calendar.setTime(date);
				exito = true;
			}
			if (iso8601string.length() == 24) { // *** Con Precision de Milisegundos
				s = s.substring(0, 26) + s.substring(27);
				Date date = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ", Locale.getDefault()).parse(s);
				calendar.setTime(date);
				exito = true;
			}
			if (!exito) {
				calendar = null;
			}
		} catch (IndexOutOfBoundsException | ParseException e) {
			logger.error("IndexOutOfBoundsException | ParseException: " + e);
			calendar = null;
		}
		return calendar;
	}

	public static <T> void validarRequest(T obj) throws BaseException {
		StringBuilder sb = new StringBuilder();
		Set<ConstraintViolation<T>> violations = validator.validate(obj);
		violations.forEach(e -> sb.append(e.getPropertyPath() + ":" + e.getMessage()));
		if (!violations.isEmpty()) {
			throw new BaseException(PropertiesExternos.CODIGO_ERROR_IDF1, PropertiesExternos.MENSAJE_ERROR_IDF1,
					new Exception(sb.toString()));
		}
	}

	public static String anyObjectToJsonText(Object jsonObject) {
		ObjectMapper mapper = new ObjectMapper().enable(SerializationFeature.INDENT_OUTPUT);
		String json = "";
		try {
			json = mapper.writeValueAsString(jsonObject);
		} catch (JsonProcessingException e) {
			logger.error(e);
		}
		return json;
	}

	public static String getStringOrEmpty(String str) {
		return (str != null) ? str : Constantes.VACIO;
	}

	public static Integer getIntegerOrZero(String str) {
		Integer nbr;
		try {
			nbr = stringToInt(str);
		} catch (Exception e) {
			nbr = Constantes.CERO;
			logger.info("Error al parsear String a Integer: " + nbr);
			logger.info("Exception: " + e);
		}
		return nbr;
	}

	public static Integer getIntegerOrZero(Integer input) {
		return (input != null) ? input : Constantes.CERO;
	}

	public static int stringToInt(String str) {
		return Integer.parseInt(str);
	}

	public static String rucCCLtoBSCS() {
		return PropertiesExternos.RUC_BSCS;
	}

	public static String rucCCLtoSGA() {
		return PropertiesExternos.RUC_SGA;
	}

	public static boolean isNullOrEmpty(String str) {
		Boolean result;
		if (str != null && !str.trim().isEmpty()) {
			result = false;
		} else {
			result = true;
		}
		return result;
	}

	public static boolean hayElementosEnLista(List<?> lista) {
		return lista != null && !lista.isEmpty();
	}

		@SuppressWarnings("rawtypes")
	private static JAXBContext obtainJaxBContextFromClass(Class clas) {
		JAXBContext context;
		context = mapContexts.get(clas);
		if (context == null) {
			try {
				logger.info("Inicializando jaxcontext... para la clase " + clas.getName());
				context = JAXBContext.newInstance(clas);
				mapContexts.put(clas, context);
			} catch (Exception e) {
				logger.error("Error creando JAXBContext:", e);
			}
		}
		return context;
	}

	public static HashMap<String, String> obtenerMapConfiguracion(String cadenaValores, String delimitadorPrimario,
			String delimitadorSecundario) {
		HashMap<String, String> mapConfiguracion = new HashMap<>();
		try {
			String strRegistros = cadenaValores;
			if (strRegistros != null && !strRegistros.isEmpty()) {
				String[] arrayRegistros = strRegistros.split(delimitadorPrimario);
				for (int i = 0; i < arrayRegistros.length; i++) {
					String key = arrayRegistros[i].split(delimitadorSecundario)[0];
					String value = arrayRegistros[i].split(delimitadorSecundario)[1];
					mapConfiguracion.put(key, value);
				}
			}
		} catch (Exception e) {
			logger.error(Constantes.ERROR_OBT_MAP_CONFIG + cadenaValores + "]", e);
		}
		return mapConfiguracion;
	}

	public static Double getDouble(Object valor) {
		try {
			return Double.valueOf(valor.toString());
		} catch (Exception e) {
			logger.debug("{}", e);
			return 0.0;
		}
	}

	public static Date convertirStringADate(String fecha, String formato) {

		try {
			SimpleDateFormat formatoFecha = new SimpleDateFormat(formato);
			return formatoFecha.parse(fecha);
		} catch (Exception e) {
			logger.debug("{}", e);
			return null;
		}
	}

	public static boolean existStringSplitValue(String[] array, String value) {
		boolean existe = false;
		if (array != null) {
			for (String val : array) {
				if (val.equals(value)) {
					existe = true;
					break;
				}
			}
		}
		return existe;
	}

	public static double decimalesRound(Double valor) {
		valor = (double) Math.round(valor * 100) / 100.0;
		return valor;
	}
}
