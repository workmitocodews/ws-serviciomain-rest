package app.serviciomain.util;

import java.text.MessageFormat;
import java.util.Map;

public class PropertiesExternos {

	private PropertiesExternos() {

	}

	private static Map<String, Object> map = PropertiesManager.getProperties();

	public static final String CODIGO_EXITO = Utilitario.convertProperties(map.get("codigo.exito"));
	public static final String MENSAJE_EXITO = Utilitario.convertProperties(map.get("mensaje.funcional.consultaExito"));

	public static final String CODIGO_ERROR_IDF1 = Utilitario
			.convertProperties(map.get("serviciocorporativows.codigo.funcional.idf1"));
	public static final String MENSAJE_ERROR_IDF1 = Utilitario
			.convertProperties(map.get("serviciocorporativows.mensaje.funcional.idf1"));

	public static final String CODIGO_ERROR_IDF2 = Utilitario
			.convertProperties(map.get("serviciocorporativows.codigo.funcional.idf2"));
	public static final String MENSAJE_ERROR_IDF2 = Utilitario
			.convertProperties(map.get("serviciocorporativows.mensaje.funcional.idf2"));

	public static final String RUC_CCL = Utilitario.convertProperties(map.get("ruc.ccl"));
	public static final String RUC_BSCS = Utilitario.convertProperties(map.get("ruc.bscs"));
	public static final String RUC_SGA = Utilitario.convertProperties(map.get("ruc.sga"));

	/**** DB - CCL ****/
	public static final String DBCCL_NOMBRE = Utilitario.convertProperties(map.get("db.ccl.nombre"));
	public static final String DBCCL_OWNER = Utilitario.convertProperties(map.get("db.ccl.owner"));
	public static final String DBCCL_JNDI = Utilitario.convertProperties(map.get("db.ccl.jndi"));

	public static final String DBCCL_TIMEOUT = Utilitario.convertProperties(map.get("db.ccl.timeout"));

	public static final String DBCCL_SP_CONSULTA_ALIAS = Utilitario
			.convertProperties(map.get("db.ccl.sp.consulta.alias"));
	public static final String DBCCL_SP_CONSULTA_SERVICIO_CORP = Utilitario
			.convertProperties(map.get("db.ccl.sp.consulta.servicio.corp"));
	public static final String DBCCL_SP_CONSULTA_ACCESO = Utilitario
			.convertProperties(map.get("db.ccl.sp.consulta.acceso"));
	public static final String DBCCL_SP_CONSULTA_SERVICIO_ALIAS = Utilitario
			.convertProperties(map.get("db.ccl.sp.servicio.alias"));
	public static final String DBCCL_SP_OBTENER_SERVICIO_ALIAS = Utilitario
			.convertProperties(map.get("db.ccl.sp.obtener.alias"));
	
	public static final String DBCCL_SP_LISTA_CATALOGO = Utilitario.convertProperties(map.get("db.ccl.sp.lista.catalogo"));

	/**** DB - BSCS ****/
	public static final String DBBSCS_NOMBRE = Utilitario.convertProperties(map.get("db.bscs.nombre"));
	public static final String DBBSCS_OWNER = Utilitario.convertProperties(map.get("db.bscs.owner"));
	public static final String DBBSCS_JNDI = Utilitario.convertProperties(map.get("db.bscs.jndi"));
	public static final String DBBSCS_TIMEOUT = Utilitario.convertProperties(map.get("db.bscs.timeout"));

	public static final String DBBSCS_SP_CONSULTA_CUENTAS = Utilitario.convertProperties(map.get("db.bscs.sp.consulta.cuentas"));
	public static final String DBBSCS_SP_CONSULTA_BOLSAS = Utilitario.convertProperties(map.get("db.bscs.sp.consulta.bolsas"));
	
	public static final String DBBSCS_SP_CONSUL_LINEASWEBCORP = Utilitario.convertProperties(map.get("db.bscs.sp.consul.lineaswebcorp"));
	
	public static final String DBBSCS_SP_DETALLE_TELEFONO = Utilitario.convertProperties(map.get("db.bscs.sp.detalle.telefono"));
	public static final String DBBSCS_SP_DETALLE_LINEA = Utilitario.convertProperties(map.get("db.bscs.sp.detalle.linea"));
	public static final String DBBSCS_SP_REGISTRA_LINEAS_FAVORITAS = Utilitario.convertProperties(map.get("db.bscs.sp.registra.lineasfavoritas"));
	public static final String DBBSCS_SP_CONSULTA_LINEASFAVOWEB = Utilitario.convertProperties(map.get("db.bscs.sp.consulta.lineasfavoweb"));
	public static final String DBBSCS_SP_CONSULTA_LINEASFAVORITA = Utilitario.convertProperties(map.get("db.bscs.sp.consulta.lineasfavoritas"));

	/**** DB - SGA ****/

	public static final String DBSGA_NOMBRE = Utilitario.convertProperties(map.get("db.sga.nombre"));
	public static final String DBSGA_OWNER = Utilitario.convertProperties(map.get("db.sga.owner"));
	public static final String DBSGA_JNDI = Utilitario.convertProperties(map.get("db.sga.jndi"));
	public static final String DBSGA_TIMEOUT = Utilitario.convertProperties(map.get("db.sga.timeout"));

	public static final String DBSGA_SP_CONSULTA_CATALOGO = Utilitario
			.convertProperties(map.get("db.sga.sp.consulta.catalogo"));
	public static final String DBSGA_SP_CONSULTA_DETALLE_X_SEDE = Utilitario
			.convertProperties(map.get("db.sga.sp.consulta.detalle.x.sede"));

	/**** WS - SaldoConsumo ****/

	public static final String WSCONSULTARSALDOCONSUMOURL = Utilitario
			.convertProperties(map.get("ws.consultasaldoconsumo.cod.aplicacion"));
	public static final String WSCONSULTARSALDOCONSUMOTIMEOUT = Utilitario
			.convertProperties(map.get("ws.consultasaldoconsumo.cod.timeout"));

	public static final String LISTACODIGOERRORMIGRACION = Utilitario
			.convertProperties(map.get("codigo.error.migracion"));
	public static final String TIPOBOLSACOBERTURA = Utilitario.convertProperties(map.get("tipo.bolsa.cobertura"));
	public static final String TIPOBOLSAS = Utilitario.convertProperties(map.get("tipo.bolsas"));
	public static final String TIPOBOLSAPAQUETE = Utilitario.convertProperties(map.get("key.tipo.bolsa.paquete"));
	public static final String TIPOUNIDADRPC = Utilitario.convertProperties(map.get("key.unidad.rpc"));
	public static final String TIPOUNIDADRRSS = Utilitario.convertProperties(map.get("key.unidad.redes.sociales"));

	public static final String PROCALCULARUNIDADMMS = Utilitario.convertProperties(map.get("pro.calcular.unidad.mms"));
	public static final String PROCALCULARUNIDADSOLES = Utilitario
			.convertProperties(map.get("pro.calcular.unidad.soles"));
	public static final String PROCALCULARUNIDADMINUTOS = Utilitario
			.convertProperties(map.get("pro.calcular.unidad.minutos"));
	public static final String PROCALCULARUNIDADMEGAS = Utilitario
			.convertProperties(map.get("pro.calcular.unidad.megas"));
	public static final String PROCALCULARUNIDADSMS = Utilitario.convertProperties(map.get("pro.calcular.unidad.sms"));
	public static final String PROCALCULARUNIDADSEGUNDOS = Utilitario
			.convertProperties(map.get("pro.calcular.unidad.segundos"));
	public static final String PROCALCULARUNIDADBYTES = Utilitario
			.convertProperties(map.get("pro.calcular.unidad.bytes"));

	public static final String PROCALCULARILIMITADOSOLES = Utilitario
			.convertProperties(map.get("pro.calcular.ilimitado.soles"));
	public static final String PROCALCULARILIMITADOSEGUNDOS = Utilitario
			.convertProperties(map.get("pro.calcular.ilimitado.segundos"));
	public static final String PROCALCULARILIMITADOSMS = Utilitario
			.convertProperties(map.get("pro.calcular.ilimitado.sms"));
	public static final String PROCALCULARILIMITADOMMS = Utilitario
			.convertProperties(map.get("pro.calcular.ilimitado.mms"));
	public static final String PROCALCULARILIMITADOMB = Utilitario
			.convertProperties(map.get("pro.calcular.ilimitado.mb"));

	public static final String PROCALCULARBOLSASCODIGOPAQUETEREDESSOCIALESOMITIDAS = Utilitario
			.convertProperties(map.get("pro.calcular.bolsas.codigopaquete.redesociales.omitidas"));

	public static final String LISTAIDRECARGAPREPAGO = Utilitario
			.convertProperties(map.get("identificador.lista.recarga.prepago"));

	public static final String IDENTIFICADORREDESSOCIALES = Utilitario
			.convertProperties(map.get("redes.sociales.ilimitado.da"));

	public static final String DETALLEDESCRIPCIONMINUTO = Utilitario
			.convertProperties(map.get("detalle.descripcion.minuto"));
	public static final String DETALLEDESCRIPCIONSMS = Utilitario.convertProperties(map.get("detalle.descripcion.sms"));
	public static final String DETALLEDESCRIPCIONDATOSMB = Utilitario
			.convertProperties(map.get("detalle.descripcion.datos.mb"));

	public static final String PROFORMATOFECHAFINRECARGAR = Utilitario
			.convertProperties(map.get("pro.formato.fechas.fin.recarga"));

	public static final String PAQUETEINTERNACIONALNOMBREPAQUETEDATOS = Utilitario
			.convertProperties(map.get("paquete.internacional.nombre.paquete.datos"));
	public static final String PAQUETEINTERNACIONALNOMBREPAQUETESMS = Utilitario
			.convertProperties(map.get("paquete.internacional.nombre.paquete.sms"));
	public static final String PAQUETEINTERNACIONALNOMBREPAQUETEVOZ = Utilitario
			.convertProperties(map.get("paquete.internacional.nombre.paquete.voz"));

	public static final String KEYCOBERTURAINTERNACIONAL = Utilitario
			.convertProperties(map.get("key.cobertura.internacional"));

	public static final String PRODESCRIPCIONILIMITADO = Utilitario
			.convertProperties(map.get("pro.descripcion.ilimitado"));
	public static final String PRODESCRIPCIONDATOSGB = Utilitario
			.convertProperties(map.get("pro.descripcion.datos.gb"));
	public static final String PRODESCRIPCIONDATOSMB = Utilitario
			.convertProperties(map.get("pro.descripcion.datos.mb"));
	public static final String PRODESCRIPCIONVOZ = Utilitario.convertProperties(map.get("pro.descripcion.voz"));
	public static final String PRODESCRIPCIONSMS = Utilitario.convertProperties(map.get("pro.descripcion.sms"));

	public static final String PROTITULOPLANDATOS = Utilitario.convertProperties(map.get("pro.titulo.plan.datos"));
	public static final String PROTITULOPLANVOZ = Utilitario.convertProperties(map.get("pro.titulo.plan.voz"));
	public static final String PROTITULOPLANSMS = Utilitario.convertProperties(map.get("pro.titulo.plan.sms"));

	public static final String FORMATODECIMALMB = Utilitario.convertProperties(map.get("formato.decimal.megas"));

	/**** ERRORES TECNICOS ****/
	public static final String ERRORTECNICOTIMEOUT = Utilitario
			.convertProperties(map.get("error.mensaje.tecnico.timeout"));
	public static final String ERRORTECNICODISPONIBILIDAD = Utilitario
			.convertProperties(map.get("error.mensaje.tecnico.disponibilidad"));
	public static final String CODIGOERRORTECNICOWS = Utilitario.convertProperties(map.get("codigo.error.tecnico.ws"));
	public static final String ERRORTECNICOWS = Utilitario.convertProperties(map.get("error.mensaje.tecnico.ws"));
	public static final String ERRORSQLTIMEOUTEXCEPTION = Utilitario
			.convertProperties(map.get("error.sqltimeoutexception"));
	public static final String ERRORWS404 = Utilitario.convertProperties(map.get("error.ws.disponibilidad.404"));
	public static final String ERROR_WS_TIMEOUT_408 = Utilitario.convertProperties(map.get("error.ws.timeout.408"));
	/**** Configuraciones Homologacion Tipo Documento ****/
	public static final String STR_HOMOLOGA_TIPO_DOCUMENTO = Utilitario
			.convertProperties(map.get("serviciocorporativows.constante.homologacion.tipo.documento"));

	public static String getMessagePropertyParametros(String property, Object... params) {
		return MessageFormat.format(property, params);
	}

}
