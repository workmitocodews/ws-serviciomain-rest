package app.serviciomain.util;
import java.net.SocketTimeoutException;
import java.util.Locale;

//import pe.com.claro.eai.ws.portalclaro.comprasypagosws.util.Constantes;
//import pe.com.claro.eai.ws.portalclaro.comprasypagosws.util.ConstantesExternos;
import app.serviciomain.exception.DBException;
import app.serviciomain.exception.WSException;

public abstract class AbstractRepository {

	protected void manageToDBException(Exception e, String nombreBD, String storedProcedure) throws DBException {
		String codigo = Constantes.CODIGO_ERROR_DISPONIBILIDAD;
		String message = "$bd - $sp :";

		if (String.valueOf(e.getMessage()).toUpperCase(Locale.getDefault()).contains(Constantes.SQL_TIMEOUTEXCEPTION)) {
			codigo = Constantes.CODIGO_ERROR_TIMEOUT;
			message = "Error de timeout en $bd - $sp :";
		}

		message = message.replace("$sp", storedProcedure).replace("$bd", nombreBD).replace("SP", "FUNCTION") + " ["
				+ e.getMessage() + "]";

		throw new DBException(codigo, message, e);
	}
	protected void manageToWSException(Exception e, String nombreWS, String strMetodo) throws WSException {
		String codigo = PropertiesExternos.ERRORWS404;
		String message = PropertiesExternos.ERRORTECNICODISPONIBILIDAD;
		StringBuilder mensaje = new StringBuilder(Constantes.CADENA_VACIA);
		if (e.toString().contains(SocketTimeoutException.class.toString().replace("class", Constantes.VACIO).trim())) {
			codigo = PropertiesExternos.ERROR_WS_TIMEOUT_408;
			message = PropertiesExternos.ERRORTECNICOTIMEOUT;
		}

		//message = message.replace("$metodo", strMetodo).replace("$ws", nombreWS).replace("WS", "SERVICIO") + " ["
		//		+ e.getMessage() + "]";
		//PropertiesExternos.getMessagePropertyParametros(message,);
		mensaje.append(PropertiesExternos.getMessagePropertyParametros(message, nombreWS, strMetodo,e.getMessage()));
		throw new WSException(codigo, mensaje.toString(), e);
	}

}