package app.serviciomain.util;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

public class UtilLog {

	public static final Logger logger = LogManager.getLogger(UtilLog.class);

	/**
	 * 
	 * Métodos para los logs de SERVICIOS
	 * 
	 **/
	public void inicioServicioLog(String idTransaccion, String nombreMetodo, Object request) {
		StringBuilder mensaje = new StringBuilder(Constantes.CADENA_VACIA);
		mensaje.append(Constantes.CORCHETE_INICIO + nombreMetodo + Constantes.ESPACIO + Constantes.ID_TX
				+ Constantes.SIGNO_IGUAL + idTransaccion + Constantes.CORCHETE_FIN + Constantes.ESPACIO);
		mensaje.append(Constantes.CORCHETE_INICIO + Constantes.INICIO + Constantes.ESPACIO + nombreMetodo
				+ Constantes.SIGNO_GUION_ESPACIO + Constantes.NOMBRE_API + Constantes.WEB_SERVICE
				+ Constantes.CORCHETE_FIN);
		logger.info(mensaje);
		logger.info("Datos de entrada servicio - " + nombreMetodo + ": ");
		logger.info(UtilLog.printJson(request));
	}

	public void finallyServicioLog(String idTransaccion, String nombreMetodo, Object response, long tiempoInicio) {
		StringBuilder mensaje = new StringBuilder(Constantes.CADENA_VACIA);

		mensaje.append(Constantes.CORCHETE_INICIO + nombreMetodo + Constantes.ESPACIO + Constantes.ID_TX
				+ Constantes.SIGNO_IGUAL + idTransaccion + Constantes.CORCHETE_FIN + Constantes.ESPACIO);
		logger.info("Datos de salida servicio - " + nombreMetodo + ": ");
		logger.info(UtilLog.printJson(response));
		logger.info(mensaje);

		mensaje = new StringBuilder(Constantes.CADENA_VACIA);
		mensaje.append(Constantes.CORCHETE_INICIO + nombreMetodo + Constantes.ESPACIO + Constantes.ID_TX
				+ Constantes.SIGNO_IGUAL + idTransaccion + Constantes.CORCHETE_FIN + Constantes.ESPACIO);
		mensaje.append(Constantes.CORCHETE_INICIO + Constantes.FIN + Constantes.ESPACIO + nombreMetodo
				+ Constantes.SIGNO_GUION_ESPACIO + Constantes.NOMBRE_API + Constantes.WEB_SERVICE
				+ Constantes.CORCHETE_FIN + Constantes.SIGNO_GUION_ESPACIO + Constantes.TIEMPO_TOTAL_TRANSACCION
				+ Constantes.SIGNO_IGUAL_ESPACIO + (System.currentTimeMillis() - tiempoInicio) + Constantes.ESPACIO
				+ Constantes.MILISEGUNDOS);
		logger.info(mensaje);
	}

	public void logErrorServicioLog(Exception e, String msjTxMetodo) {
		logger.error(Constantes.NOMBRE_API + Constantes.WEB_SERVICE + " " + msjTxMetodo + "ERROR: [Exception] - ["
				+ e.getMessage() + "]", e);
	}

	public static String cursorVacioNulo(String nombreSP) {
		return "El cursor del SP: " + nombreSP + ", se encuentra nulo o vacio.";
	}

	/**
	 * 
	 * Métodos para los logs de BUSINESS
	 * 
	 **/
	public void inicioBusinessLog(String idTransaccion, String nombreMetodo) {
		logger.info(Constantes.CORCHETE_INICIO + Constantes.INICIO + Constantes.ESPACIO + Constantes.SIGNO_DOS_PUNTOS
				+ Constantes.ESPACIO + Constantes.NOMBRE_API + Constantes.WEB_SERVICE + Constantes.ESPACIO
				+ Constantes.SIGNO_GUION + Constantes.ESPACIO + nombreMetodo + Constantes.ESPACIO + Constantes.ID_TX
				+ Constantes.SIGNO_IGUAL + idTransaccion + Constantes.CORCHETE_FIN);
	}

	public void finallyBusinessLog(String idTransaccion, String nombreMetodo) {
		logger.info(Constantes.CORCHETE_INICIO + Constantes.FIN + Constantes.ESPACIO + Constantes.SIGNO_DOS_PUNTOS
				+ Constantes.ESPACIO + Constantes.NOMBRE_API + Constantes.WEB_SERVICE + Constantes.ESPACIO
				+ Constantes.SIGNO_GUION + Constantes.ESPACIO + nombreMetodo + Constantes.ESPACIO + Constantes.ID_TX
				+ Constantes.SIGNO_IGUAL + idTransaccion + Constantes.CORCHETE_FIN);
	}

	public static String inicioConsultaSPLog(String nombreMetodo, String idTransaccion, String nombreSP) {

		StringBuilder mensaje = new StringBuilder(Constantes.CADENA_VACIA);
		mensaje.append(Constantes.CORCHETE_INICIO + nombreMetodo + Constantes.ESPACIO + Constantes.ID_TX
				+ Constantes.SIGNO_IGUAL + idTransaccion + Constantes.CORCHETE_FIN + Constantes.ESPACIO);
		mensaje.append(Constantes.STORE_PROCEDURE + nombreSP);
		return mensaje.toString();
	}

	public static String finallyConsultaSPLog(String nombreMetodo, String idTransaccion, long tiempoInicio) {

		StringBuilder mensaje = new StringBuilder(Constantes.CADENA_VACIA);
		mensaje.append(Constantes.CORCHETE_INICIO + nombreMetodo + Constantes.ESPACIO + Constantes.ID_TX
				+ Constantes.SIGNO_IGUAL + idTransaccion + Constantes.CORCHETE_FIN + Constantes.ESPACIO);
		mensaje.append(Constantes.CORCHETE_INICIO + Constantes.FIN + Constantes.ESPACIO + nombreMetodo
				+ Constantes.SIGNO_GUION_ESPACIO + Constantes.CALL + Constantes.CORCHETE_FIN
				+ Constantes.SIGNO_GUION_ESPACIO + Constantes.TIEMPO_TOTAL_TRANSACCION + Constantes.SIGNO_IGUAL_ESPACIO
				+ (System.currentTimeMillis() - tiempoInicio) + Constantes.ESPACIO + Constantes.MILISEGUNDOS);
		return mensaje.toString();
	}

	public static String obtenerMensajeConexionBD(String nombreMetodo, String idTransaccion, String nombreBD,
			String nombreJNDI) {

		StringBuilder mensaje = new StringBuilder(Constantes.CADENA_VACIA);
		mensaje.append(Constantes.CORCHETE_INICIO + nombreMetodo + Constantes.ESPACIO + Constantes.ID_TX
				+ Constantes.SIGNO_IGUAL + idTransaccion + Constantes.CORCHETE_FIN + Constantes.ESPACIO);
		mensaje.append(Constantes.CONSULTA_BD + nombreBD + Constantes.COMA_ESPACIO + Constantes.JNDI + nombreJNDI);
		return mensaje.toString();
	}

	public static String obtenerMensajeFinDaoLog(String nombreMetodo, String idTransaccion, long tiempoInicio) {

		StringBuilder mensaje = new StringBuilder(Constantes.CADENA_VACIA);
		mensaje.append(Constantes.CORCHETE_INICIO + nombreMetodo + Constantes.ESPACIO + Constantes.ID_TX
				+ Constantes.SIGNO_IGUAL + idTransaccion + Constantes.CORCHETE_FIN + Constantes.ESPACIO);
		mensaje.append(Constantes.CORCHETE_INICIO + Constantes.FIN + Constantes.ESPACIO + nombreMetodo
				+ Constantes.SIGNO_GUION_ESPACIO + Constantes.DAO + Constantes.CORCHETE_FIN
				+ Constantes.SIGNO_GUION_ESPACIO + Constantes.TIEMPO_TOTAL_TRANSACCION + Constantes.SIGNO_IGUAL_ESPACIO
				+ (System.currentTimeMillis() - tiempoInicio) + Constantes.ESPACIO + Constantes.MILISEGUNDOS);
		return mensaje.toString();
	}

	public static String obtenerMensajeInicioDaoLog(String nombreMetodo, String idTransaccion) {

		StringBuilder mensaje = new StringBuilder(Constantes.CADENA_VACIA);
		mensaje.append(Constantes.CORCHETE_INICIO + nombreMetodo + Constantes.ESPACIO + Constantes.ID_TX
				+ Constantes.SIGNO_IGUAL + idTransaccion + Constantes.CORCHETE_FIN + Constantes.ESPACIO);
		mensaje.append(Constantes.CORCHETE_INICIO + Constantes.INICIO + Constantes.ESPACIO + nombreMetodo
				+ Constantes.SIGNO_GUION_ESPACIO + Constantes.DAO + Constantes.CORCHETE_FIN);
		return mensaje.toString();
	}

	public String obtenerMensajeInicioJDBCCallLog(String nombreMetodo, String idTransaccion) {

		StringBuilder mensaje = new StringBuilder(Constantes.CADENA_VACIA);
		mensaje.append(Constantes.CORCHETE_INICIO + nombreMetodo + Constantes.ESPACIO + Constantes.ID_TX
				+ Constantes.SIGNO_IGUAL + idTransaccion + Constantes.CORCHETE_FIN + Constantes.ESPACIO);
		mensaje.append(Constantes.CORCHETE_INICIO + Constantes.INICIO + Constantes.ESPACIO + nombreMetodo
				+ Constantes.SIGNO_GUION_ESPACIO + Constantes.CALL + Constantes.CORCHETE_FIN);
		return mensaje.toString();
	}

	public String obtenerMensajeFinJDBCCallLog(String nombreMetodo, String idTransaccion, long tiempoInicio) {

		StringBuilder mensaje = new StringBuilder(Constantes.CADENA_VACIA);
		mensaje.append(Constantes.CORCHETE_INICIO + nombreMetodo + Constantes.ESPACIO + Constantes.ID_TX
				+ Constantes.SIGNO_IGUAL + idTransaccion + Constantes.CORCHETE_FIN + Constantes.ESPACIO);
		mensaje.append(Constantes.CORCHETE_INICIO + Constantes.FIN + Constantes.ESPACIO + nombreMetodo
				+ Constantes.SIGNO_GUION_ESPACIO + Constantes.CALL + Constantes.CORCHETE_FIN
				+ Constantes.SIGNO_GUION_ESPACIO + Constantes.TIEMPO_TOTAL_TRANSACCION + Constantes.SIGNO_IGUAL_ESPACIO
				+ Constantes.SIGNO_GUION_ESPACIO + Constantes.CALL + Constantes.CORCHETE_FIN
				+ Constantes.SIGNO_GUION_ESPACIO + Constantes.TIEMPO_TOTAL_TRANSACCION + Constantes.SIGNO_IGUAL_ESPACIO
				+ (System.currentTimeMillis() - tiempoInicio) + Constantes.ESPACIO + Constantes.MILISEGUNDOS);
		return mensaje.toString();
	}

	public void printResponse(Object jsonObject) {
		logger.info(UtilLog.printJson(jsonObject));
	}

	public static String printJson(Object jsonObject) {
		ObjectMapper mapper = new ObjectMapper().enable(SerializationFeature.INDENT_OUTPUT);
		String json = "";
		try {
			json = mapper.writeValueAsString(jsonObject);
		} catch (JsonProcessingException e) {
			logger.error(e);
		}
		return json;
	}

	public void printStringLogInfo(String strvalor) {
		logger.info(strvalor);
	}
}
