package app.serviciomain.util;

import java.math.BigInteger;

public class Constantes {

	private Constantes() {

	}

	public static final int BUSQUEDA_MOVIL = 1;
	public static final int BUSQUEDA_FIJO = 2;
	public static final int BUSQUEDA_CLOUD = 3;

	public static final int BUSQUEDA_FIJO_SGA = 1;
	public static final int BUSQUEDA_CLOUD_SGA = 2;

	public static final int TIPO_ALIAS_MOVIL = 1;

	public static final String DE_SEDE = "DESede";
	public static final String VACIO = "";
	public static final String ESPACIO_BLANCO = " ";
	public static final String TIMED_OUT = "timed out";
	public static final String BARRA_VERTICAL = "|";
	public static final String TEXTO_DATOS_ENTRADA = "Datos de entrada: [";
	public static final String TEXTO_DATOS_SALIDA = "Datos de salida: [";
	public static final String SEPARADOR_PUNTO = ".";
	public static final String PAQUETE = "Paquete";
	public static final String TEXTO_NULL = "null";
	public static final String TEXTO_VACIO = "";

	public static final String NOMBRE_API = "serviciomainrs";
	public static final String GUION = "-";
	public static final String CADENA_CERO = "0";
	public static final String CADENA_UNO = "1";
	public static final String CADENA_DOS = "2";
	public static final String CADENA_TRES = "3";
	public static final String CADENA_CUATRO = "4";
	public static final String CADENA_SEIS = "6";
	public static final String CADENA_SIETE = "7";
	public static final String CADENA_OCHO = "8";
	public static final String CADENA_NUEVE = "9";
	public static final String CADENA_DIEZ = "10";
	public static final String CADENA_ONCE = "11";
	public static final String CADENA_DOCE = "12";
	public static final int NUMERO_CERO = 0;
	public static final int NUMERO_UNO = 1;
	public static final int NUMERO_DOS = 2;
	public static final String CODIGO_PERU_NUMERO = "51";

	public static final String CODIGO_ERROR_TIMEOUT = "-1";
	public static final String CODIGO_ERROR_DISPONIBILIDAD = "-2";
	public static final String CODIGO_CERO = "0";
	public static final int CODIGO_CERO_NUMERO = 0;
	public static final String CODIGO_EXITO = "0";
	public static final int CODIGO_EXITO_NUMERO = 0;
	public static final String CODIGO_ERROR = "-1";

	public static final String CODIGO_ERROR_TIMEOUT_OPCIONAL = "-3";
	public static final String CODIGO_ERROR_DISPONIBILIDAD_OPCIONAL = "-4";
	public static final String CODIGO_ERROR_WS = "-5";

	public static final String UNIDAD_KBPS = "KBPS";
	public static final String UNIDAD_MB = "MB";

	public static final String LOCALE_PERU = "PE";

	public static final BigInteger COD_ERROR = BigInteger.valueOf(3);
	public static final BigInteger COD_INFO = BigInteger.valueOf(1);

	public static final String CADENA_VACIA = "";
	public static final String ESPACIO = " ";
	public static final String SIGNO_IGUAL = "=";
	public static final String SIGNO_IGUAL_ESPACIO = " = ";
	public static final String SIGNO_GUION = "-";
	public static final String SIGNO_GUION_ESPACIO = " - ";
	public static final String SIGNO_DOS_PUNTOS = ":";
	public static final String SIGNO_DOS_PUNTOS_ESPACIO_RIGHT = ": ";
	public static final String PUNTO = ".";
	public static final String COMA = ",";
	public static final String PUNTO_COMA_ESPACIO_RIGHT = "; ";
	public static final String PUNTO_COMA = ";";
	public static final String COMA_ESPACIO = ", ";
	public static final String CORCHETE_INICIO = "[";
	public static final String CORCHETE_FIN = "]";
	public static final String LLAVE_INICIO = "{";
	public static final String LLAVE_FIN = "}";
	public static final String INICIO = "INICIO";
	public static final String FIN = "FIN";
	public static final String SERVICE = "SERVICE";
	public static final String WEB_SERVICE = "WS";
	public static final String DAO = "DAO";
	public static final String CALL = "JDBCCALL";
	public static final String ID_TX = "idTx";
	public static final String DATOS_ENTRADA = "Datos de entrada:\n";
	public static final String DATOS_SALIDA = "Datos de salida:\n";
	public static final String DATOS_ENTRADA_BD = "Datos de entrada: [";
	public static final String DATOS_SALIDA_BD = "Datos de salida: [";
	public static final String TIEMPO_TOTAL_TRANSACCION = "Tiempo Total de la Transaccion(ms)";
	public static final String MILISEGUNDOS = "milisegundos";
	public static final String CONSULTA_WS = "Consulta WS - endPointAddress: ";
	public static final String METODO_WS = "operacion: ";
	public static final String CONSULTA_EXITO_WS = "Se invoco con exito el WS - endPointAddress: ";
	public static final String CONSULTA_BD = "Consultando BD: ";
	public static final String JNDI = "JNDI: ";
	public static final String STORE_PROCEDURE = "Consultando SP: ";
	public static final String CONSULTA_EXITO_STORE_PROCEDURE = "Se invoco con exito el SP: ";

	public static final String DATE_FORMAT_DDMMYYYY = "dd/MM/yyyy";
	public static final String DATE_FORMAT_YYYYMMDD = "yyyy-MM-dd";

	public static final int MENOS_UNO = -1;
	public static final int CERO = 0;
	public static final int UNO = 1;
	public static final int DOS = 2;

	public static final int VEINTE = 20;

	public static final int VEINTICUATRO = 24;

	/// **** ENCODING
	public static final String DEFAULT_ENCODING_PROPERTIES = "ISO-8859-1";
	public static final String DEFAULT_ENCODING_API = "UTF-8";
	public static final String SEPARADOR_ESPACIO = " ";
	public static final String STRING_COMA = ",";
	public static final char DOSPUNTOS = ':';
	public static final char GUIONBAJO = '_';
	public static final String NO_MINUSCULA = "no";
	public static final String TT = "TT";
	public static final boolean VERDADERO = true;
	public static final boolean FALSO = false;
	public static final Object NULO = null;
	// **** Caracteres
	public static final String CHAR_PARENTESIS_IZQUIERDO = "(";
	public static final String CHAR_PARENTESIS_DERECHO = ")";
	public static final String CHAR_INTERROGACION = "?";
	public static final String CHAR_COMA = ",";
	public static final String OK = "OK";
	public static final String ERROR = "ERROR";
	public static final String DD_MM_YYYY = "dd/MM/yyyy";
	public static final String DD_MM_YYYY_HH_MM_SS = "dd/MM/yyyy HH:mm:ss";
	public static final String BOLSA_ID6 = "6";

	// *** Parametros de Auditoria - Header Request Transport
	public static final String ACCEPT = "accept";
	public static final String IDTRANSACCION = "idTransaccion";
	public static final String MSGID = "msgid";
	public static final String TIMESTAMP = "timestamp";
	public static final String USERID = "userId";
	public static final String WSIP = "wsIp";
	public static final String PID = "pid";
	public static final String IDCONTRATO = "idContrato";
	public static final String IDAPPSOLICITANTE = "idAplicacionSolicitante";
	public static final String CODCLIENTE = "codCliente";
	public static final String ORIGEN = "origen";
	public static final String CUSTCODE = "custCode";
	public static final String CORREO = "correo";
	public static final String NOMBRE = "nombre";
	public static final String TELEFONO = "telefono";
	public static final String USUARIO_APLICACION = "usuarioAplicacion";
	public static final String CONSUMER = "consumer";
	public static final String SYSTEM = "system";

	public static final String FORMATO_FECHA_DEFAULT = "dd/MM/yyyy HH:mm:ss";
	public static final String FORMATO_FECHA_SP = "yyyy-MM-dd HH:mm:ss";
	public static final String FORMATO_FECHA_Z = "yyyy-MM-dd'T'HH:mm:ss+00:00";
	public static final String TIMEOUTEXCEPTION = "Timeout";
	public static final String TIMEOUTEXCEPTION2 = "Timed out";
	public static final String PERSISTENCEEXCEPTION = "javax.persistence.PersistenceException";
	public static final String GENERICJDBCEXCEPTION = "org.hibernate.exception";
	public static final String HIBERNATEJDBCEXCEPTION = "The application must supply JDBC connections";

	public static final String STR_CONSULTARSERVICIOMOVIL = "consultarServicioMovil";
	public static final String STR_CONSULTARSERVICIOFIJO = "consultarServicioFijo";
	public static final String STR_CONSULTARSERVICIOCLOUD = "consultarServicioCloud";

	public static final String STR_CONSULTARSERVICIOSCORPORATIVO = "consultarServiciosCorporativo";

	public static final String STR_SERVICIOMOVILRESUMEN = "servicioMovilResumen";
	public static final String STR_SERVICIOMOVILLINEA = "servicioMovilLineas";
	public static final String STR_SERVICIOFIJORESUMEN = "servicioFijoResumen";
	public static final String STR_SERVICIOCLOUDRESUMEN = "servicioCloudResumen";
	public static final String STR_CONSULTARRESUMENMOVIL = "consultarResumenMovil";
	public static final String STR_CONSULTARLINEASMOVILES = "consultarLineasMoviles";
	public static final String STR_CONSULTARRESUMENFIJO = "consultarResumenFijo";
	public static final String STR_CONSULTARRESUMENCLOUD = "consultarResumenCloud";
	public static final String STR_CONSULTARACCESOCORPORATIVO = "consultarAccesoCorporativo";
	public static final String STR_CONSULTAFAVORITO = "listaFavorito";
	public static final String STR_CONSULTARALIAS = "consultaAlias";
	public static final String STR_CONSULTARDETALLEMOVIL = "consultarDetalleMovil";
	public static final String STR_CONSULTARDETALLELINEA = "consultarDetalleLinea";
	public static final String STR_CONSULTARSERVICIOALIAS = "consultaServicioAlias";
	public static final String STR_REGISTRALINEAFAVORITA = "registraLineaFavorita";
	public static final String STR_CONSULTAMOVILLINEASALDO = "consultaMovilLineaSaldo";
	public static final String STR_CONSULTAFAVORITOWEB = "listaFavoritoweb";
	
	public static final String PERSISTENCE_CONTEXT_PCC = "pe.com.claro.comprasypagosweb.data.source.pcc";
	public static final String PERSISTENCE_CONTEXT_BSCS = "pe.com.claro.jdbc.datasources.noXA.bscsDS";

	public static final String SQL_TIMEOUTEXCEPTION = "SQLTIMEOUTEXCEPTION";
	public static final int STATUS_TIME_OUT = 504;
	public static final int STATUS_DISPONIBILIDAD = 404;

	public static final String DOCUMENTO_DNI = "DNI";
	public static final String DOCUMENTO_CE = "CARNE DE EXTRANJERIA";
	public static final String DOCUMENTO_PASAPORTE = "PASAPORTE";
	public static final String DOCUMENTO_RUC = "RUC";
	public static final String DOCUMENTO_CIP = "CIP";

	public static final String TIPO_DOCUMENTO_DNI = "002";
	public static final String TIPO_DOCUMENTO_CE = "004";
	public static final String TIPO_DOCUMENTO_PASAPORTE = "006";
	public static final String TIPO_DOCUMENTO_RUC = "001";
	public static final String TIPO_DOCUMENTO_CIP = "999";

	public static final String TIPO_DOCUMENTO_DNI2 = "2";
	public static final String TIPO_DOCUMENTO_CE2 = "4";
	public static final String TIPO_DOCUMENTO_PASAPORTE2 = "1";
	public static final String TIPO_DOCUMENTO_RUC2 = "0";
	public static final String TIPO_DOCUMENTO_CIP2 = "5";

	public static final String PALOTE = "\\|";
	public static final String CODIGO_EMPLEADO = "1";
	public static final String CODIGO_SISTEMA = "2";
	public static final String PREFIJO_H = "H";
	public static final String MEGAS_CIEN = "100";
	public static final String MEGAS_DIEZ = "10";
	public static final String LINEA_PREPAGO_REQ = "1";
	public static final String LINEA_POSTPAGO_REQ = "2";

	public static final String NOMBREAPP_SRVPRE = "EAI";
	public static final String USRAPP_SRVPRE = "USRUSSD";
	public static final String DATE_FORMAT_YYYYMMDD_SIN_GUION = "yyyyMMddHHmmss";
	public static final String TIEMPO_ACTUALIZACION = "24 horas";
	public static final String ESTADO_ACTIVO = "Activo";
	public static final String STR_USRSMSCC = "USRSMSCC";

	// *********Parametros
	public static final String CANT_LICENCIA = "CANT_LICENCIA";
	public static final String CANT_SUCURSAL = "CANT_SUCURSAL";
	public static final String DESCRIPCION = "DESCRIPCION";
	public static final String DESC_WEB_FAM = "DESC_WEB_1_FAM";
	public static final String DESC_WEB_PROD = "DESC_WEB_1_PROD";
	public static final String PO_CODERROR = "PO_CODERROR";
	public static final String PO_CURSOR = "PO_CURSOR";
	public static final String PO_MSJERROR = "PO_MSJERROR";

	public static final Integer TIPO_ALIAS_CUENTA = 2;
	public static final Integer TIPO_ALIAS_SUBCUENTA = 3;

	// *********WS
	public static final String STR_NAMES_TYP_CONSULTAR_SALDO_CONSUMO = "http://claro.com.pe/eai/ws/tarificacion/consultasaldoconsumo_d1ws/types";
	public static final String STR_NAMES_BAS = "http://claro.com.pe/eai/ws/baseschema";
	public static final String STR_NAM_TYP = "typ";
	public static final String STR_NAM_BAS = "bas";
	public static final String STR_NAM_NS1 = "ns1";
	public static final String STR_NAM_NS2 = "ns2";

	// *********Consulta de Saldos
	public static final String TEXT_NACIONAL = "Nacional";
	public static final String TEXT_INTERNACIONAL = "Internacional";
	public static final String OUTPUT_VOZ = "VOZ";
	public static final String OUTPUT_SMS = "SMS";
	public static final String OUTPUT_DATOS = "DATOS";
	public static final String OUTPUT_SOLES = "SOLES";
	public static final String OUTPUT_ADICIONAL = "ADICIONAL";
	public static final String CONSTANTE_ILIMITADO = "SMS Ilimitado";
	public static final int MINUTOS = 60;
	public static final String TIPO_BOLSA_PRINCIPAL = "PRINCIPAL";
	public static final String UNIDAD_MENSAJES = "SMS";
	public static final String UNIDAD_MINUTOS = "Minutos";
	public static final String UNIDAD_MEGAS = "MB";
	public static final int CONVERSION_GB = 1024;

	// *********Textos ConsultaSaldos
	public static final String ERROR_OBT_MAP_CONFIG = "Error al obtener el Map de Configuracion: [";

}
