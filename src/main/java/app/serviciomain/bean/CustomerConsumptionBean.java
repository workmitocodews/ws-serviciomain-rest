package app.serviciomain.bean;

import java.io.Serializable;

public class CustomerConsumptionBean implements Serializable {

	private static final long serialVersionUID = 1L;
	private String tipoPaquete;
	private String id;
	private String name;
	private String chargeTypeCode;
	private String popId;
	private double consumido;
	private double saldo;
	private double total;
	private boolean isIlimitado;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getChargeTypeCode() {
		return chargeTypeCode;
	}

	public void setChargeTypeCode(String chargeTypeCode) {
		this.chargeTypeCode = chargeTypeCode;
	}

	public boolean isIlimitado() {
		return isIlimitado;
	}

	public void setIlimitado(boolean isIlimitado) {
		this.isIlimitado = isIlimitado;
	}

	public double getConsumido() {
		return consumido;
	}

	public void setConsumido(double consumido) {
		this.consumido = consumido;
	}

	public double getSaldo() {
		return saldo;
	}

	public void setSaldo(double saldo) {
		this.saldo = saldo;
	}

	public double getTotal() {
		return total;
	}

	public void setTotal(double total) {
		this.total = total;
	}

	public String getTipoPaquete() {
		return tipoPaquete;
	}

	public void setTipoPaquete(String tipoPaquete) {
		this.tipoPaquete = tipoPaquete;
	}

	public String getPopId() {
		return popId;
	}

	public void setPopId(String popId) {
		this.popId = popId;
	}

	@Override
	public String toString() {
		return "CustomerConsumptionBean [id=" + id + ", name=" + name + ", chargeTypeCode=" + chargeTypeCode
				+ ", consumido=" + consumido + ", saldo=" + saldo + ", total=" + total + ", isIlimitado=" + isIlimitado
				+ "]";
	}
}
