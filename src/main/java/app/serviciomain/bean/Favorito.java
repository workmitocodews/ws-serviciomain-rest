package app.serviciomain.bean;

public class Favorito {
	private String linea;
	private String alias;
	private String cuenta;
	private String subcuenta;

	public String getLinea() {
		return linea;
	}

	public void setLinea(String linea) {
		this.linea = linea;
	}

	public String getCuenta() {
		return cuenta;
	}

	public void setCuenta(String cuenta) {
		this.cuenta = cuenta;
	}

	public String getSubcuenta() {
		return subcuenta;
	}

	public void setSubcuenta(String subcuenta) {
		this.subcuenta = subcuenta;
	}

	public String getAlias() {
		return alias;
	}

	public void setAlias(String alias) {
		this.alias = alias;
	}
}
