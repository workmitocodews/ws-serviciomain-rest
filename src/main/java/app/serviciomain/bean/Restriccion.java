package app.serviciomain.bean;

import java.io.Serializable;

public class Restriccion implements Serializable {

	private static final long serialVersionUID = 1L;
	private String identificador;
	private String campo;

	public String getIdentificador() {
		return identificador;
	}

	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}

	public String getCampo() {
		return campo;
	}

	public void setCampo(String campo) {
		this.campo = campo;
	}

	@Override
	public String toString() {
		return "Restriccion [identificador=" + identificador + ", campo=" + campo + "]";
	}

}
