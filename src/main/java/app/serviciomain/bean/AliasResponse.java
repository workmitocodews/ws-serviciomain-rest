package app.serviciomain.bean;

import java.util.List;

public class AliasResponse extends BaseBean {
	private List<Alias> listaAliasCorporativo;

	public List<Alias> getListaAliasCorporativo() {
		return listaAliasCorporativo;
	}

	public void setListaAliasCorporativo(List<Alias> listaAliasCorporativo) {
		this.listaAliasCorporativo = listaAliasCorporativo;
	}
}
