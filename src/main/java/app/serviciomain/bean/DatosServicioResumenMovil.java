package app.serviciomain.bean;

public class DatosServicioResumenMovil {

	public String getDesServicio() {
		return desServicio;
	}
	public void setDesServicio(String desServicio) {
		this.desServicio = desServicio;
	}
	public String getFlag() {
		return flag;
	}
	public void setFlag(String flag) {
		this.flag = flag;
	}
	public Integer getLineasActivas() {
		return lineasActivas;
	}
	public void setLineasActivas(Integer lineasActivas) {
		this.lineasActivas = lineasActivas;
	}
	public Integer getLineasAsociadas() {
		return lineasAsociadas;
	}
	public void setLineasAsociadas(Integer lineasAsociadas) {
		this.lineasAsociadas = lineasAsociadas;
	}
	public Integer getLineasBloqueadas() {
		return lineasBloqueadas;
	}
	public void setLineasBloqueadas(Integer lineasBloqueadas) {
		this.lineasBloqueadas = lineasBloqueadas;
	}
	public Integer getLineasSuspendidas() {
		return lineasSuspendidas;
	}
	public void setLineasSuspendidas(Integer lineasSuspendidas) {
		this.lineasSuspendidas = lineasSuspendidas;
	}
	public Integer getPlanes() {
		return planes;
	}
	public void setPlanes(Integer planes) {
		this.planes = planes;
	}
	public String getCicloFacturacion() {
		return cicloFacturacion;
	}
	public void setCicloFacturacion(String cicloFacturacion) {
		this.cicloFacturacion = cicloFacturacion;
	}
	private String desServicio;
	private String flag;	
	private Integer lineasActivas;
	private Integer lineasAsociadas;
	private Integer lineasBloqueadas;
	private Integer lineasSuspendidas;
	private Integer planes;
	private String cicloFacturacion;
}
