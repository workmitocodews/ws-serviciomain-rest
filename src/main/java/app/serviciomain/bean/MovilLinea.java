package app.serviciomain.bean;

public class MovilLinea {

	private String linea;
	private String estadoContrato;
	private String tipoPlan;
	private String favorita;
	private String bloqueSuspendido;
	private String ofertaProducto;
	private String bolsa;
	private Integer contrato;
	private String alias;

	public String getLinea() {
		return linea;
	}

	public void setLinea(String linea) {
		this.linea = linea;
	}

	public String getEstadoContrato() {
		return estadoContrato;
	}

	public void setEstadoContrato(String estadoContrato) {
		this.estadoContrato = estadoContrato;
	}

	public String getTipoPlan() {
		return tipoPlan;
	}

	public void setTipoPlan(String tipoPlan) {
		this.tipoPlan = tipoPlan;
	}

	public String getFavorita() {
		return favorita;
	}

	public void setFavorita(String favorita) {
		this.favorita = favorita;
	}

	public String getBloqueSuspendido() {
		return bloqueSuspendido;
	}

	public void setBloqueSuspendido(String bloqueSusp) {
		this.bloqueSuspendido = bloqueSusp;
	}

	public String getBolsa() {
		return bolsa;
	}

	public void setBolsa(String bolsa) {
		this.bolsa = bolsa;
	}

	public Integer getContrato() {
		return contrato;
	}

	public void setContrato(Integer coId) {
		this.contrato = coId;
	}

	public String getAlias() {
		return alias;
	}

	public void setAlias(String alias) {
		this.alias = alias;
	}

	public String getOfertaProducto() {
		return ofertaProducto;
	}

	public void setOfertaProducto(String ofertaProducto) {
		this.ofertaProducto = ofertaProducto;
	}

}
