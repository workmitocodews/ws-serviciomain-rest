package app.serviciomain.bean;

/**
 * @author EXPERIS
 *
 */
public class AliasLinea {

	private String lineaMovil;
	private String aliasAsociado;
	private String codCuenta;
	private String codRecibo;

	public String getLineaMovil() {
		return lineaMovil;
	}

	public void setLineaMovil(String lineaMovil) {
		this.lineaMovil = lineaMovil;
	}

	public String getAliasAsociado() {
		return aliasAsociado;
	}

	public void setAliasAsociado(String aliasAsociado) {
		this.aliasAsociado = aliasAsociado;
	}

	public String getCodCuenta() {
		return codCuenta;
	}

	public void setCodCuenta(String codCuenta) {
		this.codCuenta = codCuenta;
	}

	public String getCodRecibo() {
		return codRecibo;
	}

	public void setCodRecibo(String codRecibo) {
		this.codRecibo = codRecibo;
	}
}
