package app.serviciomain.bean;

import java.util.List;

public class MovilResponse extends BaseBean {
	private Integer totalCuentas;
	private Integer totalLineas;
	private Integer totalBolsas;
	private Integer totalPlanes;
	private List<Movil> listaCuentasMoviles;

	public Integer getTotalCuentas() {
		return totalCuentas;
	}

	public void setTotalCuentas(Integer totalCuentas) {
		this.totalCuentas = totalCuentas;
	}

	public Integer getTotalLineas() {
		return totalLineas;
	}

	public void setTotalLineas(Integer totalLineas) {
		this.totalLineas = totalLineas;
	}

	public Integer getTotalBolsas() {
		return totalBolsas;
	}

	public void setTotalBolsas(Integer totalBolsas) {
		this.totalBolsas = totalBolsas;
	}

	public Integer getTotalPlanes() {
		return totalPlanes;
	}

	public void setTotalPlanes(Integer totalPlanes) {
		this.totalPlanes = totalPlanes;
	}

	public List<Movil> getListaCuentasMoviles() {
		return listaCuentasMoviles;
	}

	public void setListaCuentasMoviles(List<Movil> listaCuentasMoviles) {
		this.listaCuentasMoviles = listaCuentasMoviles;
	}

}
