package app.serviciomain.bean;

import java.util.List;

public class MovilResumenResponse extends BaseBean {
	private List<MovilResumen> listaMovilResumen;

	public List<MovilResumen> getListaMovilResumen() {
		return listaMovilResumen;
	}

	public void setListaMovilResumen(List<MovilResumen> listaMovilResumen) {
		this.listaMovilResumen = listaMovilResumen;
	}

}
