package app.serviciomain.bean;

import java.util.List;

public class MovilLineaResponse extends BaseBean {
	private List<MovilLinea> listaMovilLinea;

	public List<MovilLinea> getListaMovilLinea() {
		return listaMovilLinea;
	}

	public void setListaMovilLinea(List<MovilLinea> listaMovilLinea) {
		this.listaMovilLinea = listaMovilLinea;
	}
}
