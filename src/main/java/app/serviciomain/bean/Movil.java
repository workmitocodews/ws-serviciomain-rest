package app.serviciomain.bean;

public class Movil {
	private String customerId;
	private String custcode;
	private Integer idType;
	private Integer lineas;
	private Integer planes;
	private Integer bolsa;
	private Integer nivel;
	private String cuentaPadre;
	private Integer tipoAlias;
	private String cicloFacturacion;

	public Integer getNivel() {
		return nivel;
	}

	public void setNivel(Integer nivel) {
		this.nivel = nivel;
	}

	public String getCuentaPadre() {
		return cuentaPadre;
	}

	public void setCuentaPadre(String cuentaPadre) {
		this.cuentaPadre = cuentaPadre;
	}

	// SE OBTIENE DE LA CCLDB
	private Alias alias;

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getCustcode() {
		return custcode;
	}

	public void setCustcode(String custcode) {
		this.custcode = custcode;
	}

	public Integer getIdType() {
		return idType;
	}

	public void setIdType(Integer idType) {
		this.idType = idType;
	}

	public Integer getLineas() {
		return lineas;
	}

	public void setLineas(Integer lineas) {
		this.lineas = lineas;
	}

	public Integer getPlanes() {
		return planes;
	}

	public void setPlanes(Integer planes) {
		this.planes = planes;
	}

	public Integer getBolsa() {
		return bolsa;
	}

	public void setBolsa(Integer bolsa) {
		this.bolsa = bolsa;
	}

	public Alias getAlias() {
		return alias;
	}

	public void setAlias(Alias alias) {
		this.alias = alias;
	}

	public Integer getTipoAlias() {
		return tipoAlias;
	}

	public void setTipoAlias(Integer tipoAlias) {
		this.tipoAlias = tipoAlias;
	}

	public String getCicloFacturacion() {
		return cicloFacturacion;
	}

	public void setCicloFacturacion(String cicloFacturacion) {
		this.cicloFacturacion = cicloFacturacion;
	}
}
