package app.serviciomain.bean;

public class DefaultServiceResponse {
	protected String idRespuesta;
	protected String mensaje;
	protected String excepcion;
	protected String idTransaccional;

	public String getIdRespuesta() {
		return idRespuesta;
	}

	public void setIdRespuesta(String idRespuesta) {
		this.idRespuesta = idRespuesta;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public String getExcepcion() {
		return excepcion;
	}

	public void setExcepcion(String excepcion) {
		this.excepcion = excepcion;
	}

	public String getIdTransaccional() {
		return idTransaccional;
	}

	public void setIdTransaccional(String idTransaccional) {
		this.idTransaccional = idTransaccional;
	}

}
