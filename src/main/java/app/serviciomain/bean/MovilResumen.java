package app.serviciomain.bean;

public class MovilResumen extends DatosServicioResumenMovil{
	private String customerId;
	private String custcode;
	private Integer lineas;
	private String lineasPorPlan;
	private Integer bolsa;
	private String nomBolsa;

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getCustcode() {
		return custcode;
	}

	public void setCustcode(String custcode) {
		this.custcode = custcode;
	}

	public Integer getLineas() {
		return lineas;
	}

	public void setLineas(Integer lineas) {
		this.lineas = lineas;
	}

	public String getLineasPorPlan() {
		return lineasPorPlan;
	}

	public void setLineasPorPlan(String lineasPorPlan) {
		this.lineasPorPlan = lineasPorPlan;
	}

	public Integer getBolsa() {
		return bolsa;
	}

	public void setBolsa(Integer bolsa) {
		this.bolsa = bolsa;
	}

	public String getNomBolsa() {
		return nomBolsa;
	}

	public void setNomBolsa(String nomBolsa) {
		this.nomBolsa = nomBolsa;
	}

}
