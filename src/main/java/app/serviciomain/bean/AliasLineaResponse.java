package app.serviciomain.bean;

import java.util.List;

public class AliasLineaResponse extends BaseBean {

	private List<app.serviciomain.bean.AliasLinea> listaAliasLineaCorporativo;

	public List<AliasLinea> getListaAliasLineaCorporativo() {
		return listaAliasLineaCorporativo;
	}

	public void setListaAliasLineaCorporativo(List<app.serviciomain.bean.AliasLinea> listaAliasLineaCorporativo) {
		this.listaAliasLineaCorporativo = listaAliasLineaCorporativo;
	}
}
