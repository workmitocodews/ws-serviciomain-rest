package app.serviciomain.bean;

import java.util.List;

public class FavoritaLineaResponse extends BaseBean {

	private List<FavoritaLinea> listaFavoritaLineaCorporativo;

	public List<FavoritaLinea> getListaFavoritaLineaCorporativo() {
		return listaFavoritaLineaCorporativo;
	}

	public void setListaFavoritaLineaCorporativo(List<FavoritaLinea> listaFavoritaLineaCorporativo) {
		this.listaFavoritaLineaCorporativo = listaFavoritaLineaCorporativo;
	}

}
