package app.serviciomain.bean;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize
public class Alias {
	private String custcode;
	private String descripcion;
	@JsonProperty("alias")
	private String descAlias;

	public String getCustcode() {
		return custcode;
	}

	public void setCustcode(String custcode) {
		this.custcode = custcode;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getAlias() {
		return descAlias;
	}

	public void setAlias(String descAlias) {
		this.descAlias = descAlias;
	}
}
