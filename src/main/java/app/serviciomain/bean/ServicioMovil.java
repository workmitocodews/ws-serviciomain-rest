package app.serviciomain.bean;

public class ServicioMovil extends DatosServicioResumenMovil{

	private Integer totalLineas;

	public Integer getTotalLineas() {
		return totalLineas;
	}

	public void setTotalLineas(Integer totalLineas) {
		this.totalLineas = totalLineas;
	}

}
