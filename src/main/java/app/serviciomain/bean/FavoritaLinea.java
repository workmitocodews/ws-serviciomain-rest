package app.serviciomain.bean;

public class FavoritaLinea {

	private String linea;
	private String cuenta;
	private String subCuenta;

	public String getLinea() {
		return linea;
	}

	public void setLinea(String lineaMovil) {
		this.linea = lineaMovil;
	}

	public String getCuenta() {
		return cuenta;
	}

	public void setCuenta(String isFavorita) {
		this.cuenta = isFavorita;
	}

	public String getSubCuenta() {
		return subCuenta;
	}

	public void setSubCuenta(String codCuenta) {
		this.subCuenta = codCuenta;
	}

}
