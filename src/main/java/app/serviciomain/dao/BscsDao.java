package app.serviciomain.dao;

import javax.ejb.Local;

import app.serviciomain.exception.DBException;
import app.serviciomain.bean.DefaultServiceResponse;
import app.serviciomain.bean.FavoritaLineaResponse;
import app.serviciomain.bean.MovilLineaResponse;
import app.serviciomain.bean.MovilResponse;
import app.serviciomain.bean.MovilResumenResponse;
import app.serviciomain.types.DetalleMovilRequest;
import app.serviciomain.types.DetalleMovilResponse;
import app.serviciomain.types.RegistraLineaFavoritaRequest;
import app.serviciomain.types.ServicioMovilResumenRequest;
import app.serviciomain.types.ServicioRequestBase;
import app.serviciomain.types.ConsultaFavoritoResponse;
import app.serviciomain.types.DetalleLineaResponse;

@Local
public interface BscsDao {

	MovilResponse consultaServiciosMoviles(ServicioRequestBase request) throws DBException;

	MovilResumenResponse consultaServiciosMovilesResumen(ServicioMovilResumenRequest request) throws DBException;

	MovilLineaResponse consultaServiciosMovilesLineas(ServicioMovilResumenRequest request) throws DBException;

	DetalleMovilResponse consultaDetalleMovil(DetalleMovilRequest request) throws DBException;

	ConsultaFavoritoResponse consultaFavorito(ServicioRequestBase request) throws DBException;

	DefaultServiceResponse registraLineaFavorita(RegistraLineaFavoritaRequest request) throws DBException;

	FavoritaLineaResponse consultaObtenerFavorito(ServicioMovilResumenRequest request) throws DBException;
	
	DetalleLineaResponse consultaDetalleLinea(DetalleMovilRequest request) throws DBException;

}
