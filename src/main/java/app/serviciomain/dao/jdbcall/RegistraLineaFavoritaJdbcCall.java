package app.serviciomain.dao.jdbcall;

import java.io.Serializable;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;

import javax.sql.DataSource;

import app.serviciomain.bean.DefaultServiceResponse;
import app.serviciomain.exception.DBException;
import app.serviciomain.types.RegistraLineaFavoritaRequest;
import app.serviciomain.util.AbstractRepository;
import app.serviciomain.util.Constantes;
import app.serviciomain.util.PropertiesExternos;
import app.serviciomain.util.UtilLog;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

public class RegistraLineaFavoritaJdbcCall extends AbstractRepository implements Serializable {

	private static final long serialVersionUID = 1L;
	private final transient Logger logger = LogManager.getLogger(this.getClass());
	private static final String NOMBRE_SCHEMA = PropertiesExternos.DBBSCS_OWNER;
	transient DataSource dt;

	public RegistraLineaFavoritaJdbcCall(DataSource dt) {
		this.dt = dt;
	}

	public DefaultServiceResponse registraLineaFavorita(RegistraLineaFavoritaRequest request) throws DBException {
		logger.info("Ejecutando RegistraLineaFavorita");
		long tiempoInicio = System.currentTimeMillis();
		String numeroDocumento = request.getNumeroDocumento();
		String tipoDocumento = request.getTipoDocumento();
		String idTransaccion = request.getIdTransaccion();
		String numeroCuenta = request.getNumeroCuenta();
		String numeroMovil = request.getNumeroMovil();

		DefaultServiceResponse response = new DefaultServiceResponse();
		final StringBuilder storedProcedure = new StringBuilder();
		String nombreSP = PropertiesExternos.DBBSCS_SP_REGISTRA_LINEAS_FAVORITAS;
		String parametroSP = " (PI_LINEA=" + numeroMovil + ",PI_CUENTA=" + numeroCuenta + ",PI_TIPO_DOCUMENTO="
				+ tipoDocumento + ",PI_NRO_DOCUMENTO=" + numeroDocumento + ")";
		storedProcedure.append(NOMBRE_SCHEMA + Constantes.PUNTO + nombreSP);
		logger.info(UtilLog.inicioConsultaSPLog(Constantes.STR_REGISTRALINEAFAVORITA, request.getIdTransaccion(),
				nombreSP + parametroSP));

		try (Connection cnx = dt.getConnection();
				CallableStatement call = cnx.prepareCall("{ call " + storedProcedure + "(?,?,?,?,?,?) }");) {

			logger.info("timeout BSCS : " + PropertiesExternos.DBBSCS_TIMEOUT);
			call.setQueryTimeout(Integer.parseInt(PropertiesExternos.DBBSCS_TIMEOUT));
			call.setString("PI_LINEA", numeroMovil);
			call.setString("PI_CUENTA", numeroCuenta);
			call.setString("PI_TIPO_DOCUMENTO", tipoDocumento);
			call.setString("PI_NRO_DOCUMENTO", numeroDocumento);
			call.registerOutParameter("PO_CODERROR", java.sql.Types.INTEGER);
			call.registerOutParameter("PO_MSGERROR", java.sql.Types.VARCHAR);

			call.execute();

			response.setIdRespuesta(call.getObject("PO_CODERROR").toString());
			response.setMensaje(call.getString("PO_MSGERROR"));
			response.setIdTransaccional(idTransaccion);

		} catch (SQLException e) {
			manageToDBException(e, PropertiesExternos.DBBSCS_NOMBRE, nombreSP);
		} catch (Exception e2) {
			manageToDBException(e2, PropertiesExternos.DBBSCS_NOMBRE, nombreSP);
		} finally {
			logger.info(UtilLog.finallyConsultaSPLog(Constantes.STR_REGISTRALINEAFAVORITA, request.getIdTransaccion(),
					tiempoInicio));
		}
		return response;
	}

}
