package app.serviciomain.dao;

import javax.ejb.Local;

import app.serviciomain.bean.AliasLineaResponse;
import app.serviciomain.bean.AliasResponse;
import app.serviciomain.exception.DBException;
import app.serviciomain.types.ServicioAliasRequest;
import app.serviciomain.types.ServicioAliasResponse;
import app.serviciomain.types.ServicioMovilResumenRequest;
import app.serviciomain.types.ServicioRequestBase;
import app.serviciomain.types.ParametroConsultaRequest;
import app.serviciomain.types.ParametroConsultaResponse;

@Local
public interface CclDao {

	AliasResponse consultaAlias(ServicioRequestBase request, int tipoBusqueda) throws DBException;

	ServicioAliasResponse consultaServicioAlias(ServicioAliasRequest request) throws DBException;

	AliasLineaResponse consultaObtenerAlias(ServicioMovilResumenRequest request) throws DBException;

	ParametroConsultaResponse listarParametro(ParametroConsultaRequest request) throws DBException;
}
