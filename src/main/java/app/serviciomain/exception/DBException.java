package app.serviciomain.exception;

public class DBException extends BaseException {
	private static final long serialVersionUID = 1L;

	public DBException(String codigoError, String mensajeError, Exception e) {
		super(codigoError, mensajeError, e);
	}

}
