package app.serviciomain.exception;

public class WSException extends BaseException {

	private static final long serialVersionUID = 1L;

	public WSException(String code, String message, Exception exception) {
		super(code, message, exception);
	}

}
