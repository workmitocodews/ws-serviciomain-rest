package app.serviciomain.exception;

public class BaseException extends Exception {

	private static final long serialVersionUID = 1L;
	private final String code;

	public BaseException(String code, String message, Exception e) {
		super(message, e);
		this.code = code;
	}

	public String getCode() {
		return code;
	}

}
