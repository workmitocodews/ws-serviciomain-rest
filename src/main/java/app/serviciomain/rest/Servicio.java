package app.serviciomain.rest;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import app.serviciomain.exception.BaseException;
import app.serviciomain.util.Constantes;
import app.serviciomain.util.UtilLog;
import app.serviciomain.util.Utilitario;
import io.swagger.v3.oas.annotations.Parameter;
import app.serviciomain.bean.DefaultServiceResponse;

import app.serviciomain.bussiness.ServicioMovilBusiness;
import app.serviciomain.bussiness.ServicioMovilLineasBusiness;
import app.serviciomain.bussiness.ServicioMovilResumenBusiness;
import app.serviciomain.bussiness.FavoritoBusiness;

import app.serviciomain.types.ConsultaFavoritoResponse;
import app.serviciomain.types.DetalleMovilRequest;
import app.serviciomain.types.DetalleMovilResponse;
import app.serviciomain.types.RegistraLineaFavoritaRequest;
import app.serviciomain.types.RegistraLineaFavoritaResponse;
import app.serviciomain.types.DetalleLineaResponse;
import app.serviciomain.types.ServicioMovilLineasResponse;
import app.serviciomain.types.ServicioMovilResponse;
import app.serviciomain.types.ServicioMovilResumenRequest;
import app.serviciomain.types.ServicioMovilResumenResponse;
import app.serviciomain.types.ServicioRequestBase;

@Path("/servicios")
public class Servicio {

	@Inject
	private ServicioMovilBusiness servicioMovilBusiness;

	@Inject
	private ServicioMovilResumenBusiness servicioMovilResumenBusiness;

	@Inject
	private ServicioMovilLineasBusiness servicioMovilLineasBusiness;

	@Inject
	private FavoritoBusiness favoritoBusiness;

	@Context
	HttpHeaders httpHeaders;

	private UtilLog utilLog;
	public Servicio() {
		utilLog = new UtilLog();
	}

	@POST
	@Path("/movil")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON + ";charset=UTF-8" })
	public Response serviciosMoviles(
			@Parameter(description = "El objeto request", required = true) ServicioRequestBase request) {

		request.setIdTransaccion(httpHeaders.getHeaderString(Constantes.IDTRANSACCION));
		String nombreMetodo = Constantes.STR_CONSULTARSERVICIOMOVIL;
		ServicioMovilResponse responseMovil = new ServicioMovilResponse();
		long tiempoInicio = System.currentTimeMillis();
		try {
			utilLog.inicioServicioLog(request.getIdTransaccion(), nombreMetodo, request);
			Utilitario.validarRequest(request);
			responseMovil = servicioMovilBusiness.servicioMovil(request);
		} catch (BaseException e) {
			responseMovil.setDefaultServiceResponse(obtenerDefaultExceptionResponse(e, request.getIdTransaccion()));
			utilLog.logErrorServicioLog(e, Constantes.STR_CONSULTARSERVICIOMOVIL);
		} finally {
			responseMovil.getDefaultServiceResponse().setIdTransaccional(request.getIdTransaccion());
			utilLog.finallyServicioLog(request.getIdTransaccion(), nombreMetodo, responseMovil, tiempoInicio);
		}
		return Response.ok(responseMovil).build();
	}

	@POST
	@Path("/movil/resumen")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON + ";charset=UTF-8" })
	public Response servicioMovilResumen(
			@Parameter(description = "El objeto request", required = true) ServicioMovilResumenRequest request) {

		request.setIdTransaccion(httpHeaders.getHeaderString(Constantes.IDTRANSACCION));
		String nombreMetodo = Constantes.STR_SERVICIOMOVILRESUMEN;
		ServicioMovilResumenResponse responseMovilResumen = new ServicioMovilResumenResponse();
		long tiempoInicio = System.currentTimeMillis();
		try {
			utilLog.inicioServicioLog(request.getIdTransaccion(), nombreMetodo, request);
			Utilitario.validarRequest(request);
			responseMovilResumen = servicioMovilResumenBusiness.servicioMovilResumen(request);
		} catch (BaseException e) {
			responseMovilResumen.setDefaultServiceResponse(obtenerDefaultExceptionResponse(e, request.getIdTransaccion()));
			utilLog.logErrorServicioLog(e, Constantes.STR_SERVICIOMOVILRESUMEN);
		} finally {
			responseMovilResumen.getDefaultServiceResponse().setIdTransaccional(request.getIdTransaccion());
			utilLog.finallyServicioLog(request.getIdTransaccion(), nombreMetodo, responseMovilResumen, tiempoInicio);
		}
		return Response.ok(responseMovilResumen).build();
	}


	@GET
	@Path("/movil/detalle")
	@Produces({ MediaType.APPLICATION_JSON + ";charset=UTF-8" })
	public Response consultarDetalleMovil(@QueryParam(value = "numeroDocumento") String numeroDocumento,
			@QueryParam(value = "tipoDocumento") String tipoDocumento,
			@QueryParam(value = "numeroMovil") String numeroMovil) {

		DetalleMovilRequest request = new DetalleMovilRequest();

		request.setIdTransaccion(httpHeaders.getHeaderString(Constantes.IDTRANSACCION));
		request.setTipoDocumento(tipoDocumento);
		request.setNumeroDocumento(numeroDocumento);
		request.setValorServicio(numeroMovil);

		String nombreMetodo = Constantes.STR_CONSULTARDETALLEMOVIL;
		DetalleMovilResponse responseMovilDetalle = new DetalleMovilResponse();
		long tiempoInicio = System.currentTimeMillis();

		try {
			utilLog.inicioServicioLog(request.getIdTransaccion(), nombreMetodo, request);
			Utilitario.validarRequest(request);
			responseMovilDetalle = servicioMovilBusiness.servicioDetalleMovil(request);
			return Response.ok(responseMovilDetalle).build();
		} catch (BaseException e1) {
			responseMovilDetalle.setDefaultServiceResponse(obtenerDefaultExceptionResponse(e1, request.getIdTransaccion()));
			utilLog.logErrorServicioLog(e1, Constantes.STR_CONSULTARDETALLEMOVIL);
		} finally {
			utilLog.finallyServicioLog(request.getIdTransaccion(), nombreMetodo, responseMovilDetalle, tiempoInicio);
		}
		return Response.ok(responseMovilDetalle).build();

	}
	
	@GET
	@Path("/linea/detalle")
	@Produces({ MediaType.APPLICATION_JSON + ";charset=UTF-8" })
	public Response consultarDetalleLinea(@QueryParam(value = "numeroDocumento") String numeroDocumento,
			@QueryParam(value = "tipoDocumento") String tipoDocumento,
			@QueryParam(value = "numeroMovil") String numeroMovil) {

		DetalleMovilRequest request = new DetalleMovilRequest();

		request.setIdTransaccion(httpHeaders.getHeaderString(Constantes.IDTRANSACCION));
		request.setTipoDocumento(tipoDocumento);
		request.setNumeroDocumento(numeroDocumento);
		request.setValorServicio(numeroMovil);

		String nombreMetodo = Constantes.STR_CONSULTARDETALLELINEA;
		DetalleLineaResponse responseDetalleLinea = new DetalleLineaResponse();
		long tiempoInicio = System.currentTimeMillis();

		try {
			utilLog.inicioServicioLog(request.getIdTransaccion(), nombreMetodo, request);
			Utilitario.validarRequest(request);
			responseDetalleLinea = servicioMovilBusiness.servicioDetalleLinea(request);
			return Response.ok(responseDetalleLinea).build();
		} catch (BaseException e1) {
			responseDetalleLinea.setDefaultServiceResponse(obtenerDefaultExceptionResponse(e1, request.getIdTransaccion()));
			utilLog.logErrorServicioLog(e1, Constantes.STR_CONSULTARDETALLELINEA);
		} finally {
			utilLog.finallyServicioLog(request.getIdTransaccion(), nombreMetodo, responseDetalleLinea, tiempoInicio);
		}
		return Response.ok(responseDetalleLinea).build();

	}
	@GET
	@Path("/favorito/lista")
	@Produces({ MediaType.APPLICATION_JSON + ";charset=UTF-8" })
	public Response listaFavorito(@Parameter(description = "El numeroDocumento/TipoDocumento del usuario", required = false) 
										@QueryParam(value = "numeroDocumento") String nrodocumento,
										@QueryParam("tipoDocumento") String tipdocumento) {

		ServicioRequestBase request = new ServicioRequestBase();
		request.setNumeroDocumento(nrodocumento);
		request.setTipoDocumento(tipdocumento);
		request.setIdTransaccion(httpHeaders.getHeaderString(Constantes.IDTRANSACCION));
		ConsultaFavoritoResponse responseListaFavorito = new ConsultaFavoritoResponse();
		String nombreMetodo = "Metodo[GET] .../servicios/Favorito/lista - " + Constantes.STR_CONSULTAFAVORITO;
		long tiempoInicio = System.currentTimeMillis();
		try {
			utilLog.inicioServicioLog(request.getIdTransaccion(), nombreMetodo, request);
			responseListaFavorito = favoritoBusiness.listaFavorito(request);
		} catch (BaseException e) {

			responseListaFavorito.setIdRespuesta(e.getCode());
			responseListaFavorito.setExcepcion(e.toString());
			responseListaFavorito.setMensaje(e.getMessage());
			responseListaFavorito.setIdTransaccional(request.getIdTransaccion());
			utilLog.logErrorServicioLog(e, nombreMetodo);
		} finally {
			utilLog.finallyServicioLog(request.getIdTransaccion(), nombreMetodo, responseListaFavorito, tiempoInicio);
		}
		return Response.ok(responseListaFavorito).build();

	}

	@GET
	@Path("/movil/listarMovilCuenta")
	@Produces({ MediaType.APPLICATION_JSON + ";charset=UTF-8" })
	public Response servicioMovilLineas(@QueryParam(value = "numeroDocumento") String numeroDocumento,
			@QueryParam(value = "tipoDocumento") String tipoDocumento,
			@QueryParam(value = "numeroCuenta") String numeroCuenta, @QueryParam(value = "nivel") String nivel) {

		ServicioMovilResumenRequest request = new ServicioMovilResumenRequest();

		request.setIdTransaccion(httpHeaders.getHeaderString(Constantes.IDTRANSACCION));
		request.setNivel(nivel);
		request.setNumeroCuenta(numeroCuenta);
		request.setNumeroDocumento(numeroDocumento);
		request.setTipoDocumento(tipoDocumento);

		String nombreMetodo = Constantes.STR_SERVICIOMOVILLINEA;

		ServicioMovilLineasResponse responseListarMovilCta = new ServicioMovilLineasResponse();
		long tiempoInicio = System.currentTimeMillis();
		try {
			utilLog.inicioServicioLog(request.getIdTransaccion(), nombreMetodo, request);
			Utilitario.validarRequest(request);
			responseListarMovilCta = servicioMovilLineasBusiness.servicioMovilLineas(request);
		} catch (BaseException e) {
			responseListarMovilCta.setDefaultServiceResponse(obtenerDefaultExceptionResponse(e, request.getIdTransaccion()));
			utilLog.logErrorServicioLog(e, Constantes.STR_SERVICIOMOVILLINEA);
		} finally {
			responseListarMovilCta.getDefaultServiceResponse().setIdTransaccional(request.getIdTransaccion());
			utilLog.finallyServicioLog(request.getIdTransaccion(), nombreMetodo, responseListarMovilCta, tiempoInicio);
		}
		return Response.ok(responseListarMovilCta).build();
	}

	@POST
	@Path("/favorito/registro")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON + ";charset=UTF-8" })
	public Response registraLineaFavorita(
			@Parameter(description = "El objeto request", required = true) RegistraLineaFavoritaRequest request) {

		request.setIdTransaccion(httpHeaders.getHeaderString(Constantes.IDTRANSACCION));
		String nombreMetodo = Constantes.STR_REGISTRALINEAFAVORITA;
		RegistraLineaFavoritaResponse response = new RegistraLineaFavoritaResponse(); 
		long tiempoInicio = System.currentTimeMillis();
		try {
			utilLog.inicioServicioLog(request.getIdTransaccion(), nombreMetodo, request);
			Utilitario.validarRequest(request);
			response.setDefaultServiceResponse(favoritoBusiness.registraLineaFavorita(request));
		} catch (BaseException e) {
			response.setDefaultServiceResponse(obtenerDefaultExceptionResponse(e, request.getIdTransaccion()));
			utilLog.logErrorServicioLog(e, Constantes.STR_CONSULTARACCESOCORPORATIVO);
		} finally {
			utilLog.finallyServicioLog(request.getIdTransaccion(), nombreMetodo, response, tiempoInicio);
		}
		return Response.ok(response).build();
	}

		private DefaultServiceResponse obtenerDefaultExceptionResponse(BaseException e, String idTransacion) {
		DefaultServiceResponse exceptionResponse = new DefaultServiceResponse();
		exceptionResponse.setIdRespuesta(e.getCode());
		exceptionResponse.setExcepcion(e.toString());
		exceptionResponse.setMensaje(e.getMessage());
		exceptionResponse.setIdTransaccional(idTransacion);
		return exceptionResponse;
	}
}