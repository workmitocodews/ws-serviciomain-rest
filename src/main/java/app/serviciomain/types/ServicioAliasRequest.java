package app.serviciomain.types;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class ServicioAliasRequest extends ServicioRequestBase {

	@NotNull
	@Size(min = 1)
	private String valorServicio;
	@NotNull
	private Integer tipoAlias;

	public Integer getTipoAlias() {
		return tipoAlias;
	}

	public void setTipoAlias(Integer tipoAlias) {
		this.tipoAlias = tipoAlias;
	}

	public String getValorServicio() {
		return valorServicio;
	}

	public void setValorServicio(String valorServicio) {
		this.valorServicio = valorServicio;
	}

}
