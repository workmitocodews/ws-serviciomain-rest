package app.serviciomain.types;

import java.util.List;

import app.serviciomain.bean.DefaultServiceResponse;
import app.serviciomain.bean.ServicioMovil;

public class ServicioMovilResumenResponse {
	protected DefaultServiceResponse defaultServiceResponse;
	private String custcode;
	private String customerId;
	private List<ServicioMovil> listaResumenMoviles;

	public DefaultServiceResponse getDefaultServiceResponse() {
		return defaultServiceResponse;
	}

	public void setDefaultServiceResponse(DefaultServiceResponse defaultServiceResponse) {
		this.defaultServiceResponse = defaultServiceResponse;
	}

	public String getCustcode() {
		return custcode;
	}

	public void setCustcode(String custcode) {
		this.custcode = custcode;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public List<ServicioMovil> getListaResumenMoviles() {
		return listaResumenMoviles;
	}

	public void setListaResumenMoviles(List<ServicioMovil> listaResumenMoviles) {
		this.listaResumenMoviles = listaResumenMoviles;
	}
}
