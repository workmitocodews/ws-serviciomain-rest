package app.serviciomain.types;

public class DetalleLineaResponse extends DetalleMovilResponse {
	private String tipoLinea;
	private String infoTipoLinea;
	private String descripcionBono;
	private String vigenciaBono;
	private String descripcionServicioBono;

	public String getTipoLinea() {
		return tipoLinea;
	}

	public void setTipoLinea(String tipoLinea) {
		this.tipoLinea = tipoLinea;
	}

	public String getInfoTipoLinea() {
		return infoTipoLinea;
	}

	public void setInfoTipoLinea(String infoTipoLinea) {
		this.infoTipoLinea = infoTipoLinea;
	}

	public String getDescripcionBono() {
		return descripcionBono;
	}

	public void setDescripcionBono(String descripcionBono) {
		this.descripcionBono = descripcionBono;
	}

	public String getVigenciaBono() {
		return vigenciaBono;
	}

	public void setVigenciaBono(String vigenciaBono) {
		this.vigenciaBono = vigenciaBono;
	}

	public String getDescripcionServicioBono() {
		return descripcionServicioBono;
	}

	public void setDescripcionServicioBono(String descripcionServicioBono) {
		this.descripcionServicioBono = descripcionServicioBono;
	}
}
