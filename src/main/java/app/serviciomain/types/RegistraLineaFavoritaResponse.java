package app.serviciomain.types;

import app.serviciomain.bean.DefaultServiceResponse;

public class RegistraLineaFavoritaResponse {
	
	protected DefaultServiceResponse defaultServiceResponse;

	public DefaultServiceResponse getDefaultServiceResponse() {
		return defaultServiceResponse;
	}

	public void setDefaultServiceResponse(DefaultServiceResponse defaultServiceResponse) {
		this.defaultServiceResponse = defaultServiceResponse;
	}

}
