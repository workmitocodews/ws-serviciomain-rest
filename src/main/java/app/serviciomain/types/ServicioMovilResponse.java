package app.serviciomain.types;

import app.serviciomain.bean.DefaultServiceResponse;
import app.serviciomain.bean.MovilResponse;

public class ServicioMovilResponse {
	protected DefaultServiceResponse defaultServiceResponse;
	protected MovilResponse movilResponse;

	public MovilResponse getMovilResponse() {
		return movilResponse;
	}

	public void setMovilResponse(MovilResponse movilResponse) {
		this.movilResponse = movilResponse;
	}

	public DefaultServiceResponse getDefaultServiceResponse() {
		return defaultServiceResponse;
	}

	public void setDefaultServiceResponse(DefaultServiceResponse defaultServiceResponse) {
		this.defaultServiceResponse = defaultServiceResponse;
	}

}
