package app.serviciomain.types;

import javax.validation.constraints.NotEmpty;

public class ServicioMovilResumenRequest extends ServicioRequestBase {
	@NotEmpty
	protected String numeroCuenta;
	protected String nivel;

	public String getNumeroCuenta() {
		return numeroCuenta;
	}

	public void setNumeroCuenta(String numeroCuenta) {
		this.numeroCuenta = numeroCuenta;
	}

	public String getNivel() {
		return nivel;
	}

	public void setNivel(String nivel) {
		this.nivel = nivel;
	}
}
