package app.serviciomain.types;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class DetalleMovilRequest extends ServicioRequestBase {

	@NotNull
	@Size(min = 1)
	private String valorServicio;

	public String getValorServicio() {
		return valorServicio;
	}

	public void setValorServicio(String valorServicio) {
		this.valorServicio = valorServicio;
	}

}
