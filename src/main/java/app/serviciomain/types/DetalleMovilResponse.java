package app.serviciomain.types;

import app.serviciomain.bean.DefaultServiceResponse;

public class DetalleMovilResponse {

	protected DefaultServiceResponse defaultServiceResponse;
	private String estado;
	private String customerID;
	private String cuenta;
	private String nombrePlan;
	private String codigoPlan;
	private String simCard;
	private String fechaRenovacion;
	private String roaming;
	private String numeroDocumento;
	private String aliasMovil;
	private String aliasCuenta;
	private String imei;
	private String equipoAsociado;

	public String getAliasCuenta() {
		return aliasCuenta;
	}

	public void setAliasCuenta(String aliasCuenta) {
		this.aliasCuenta = aliasCuenta;
	}

	public String getImei() {
		return imei;
	}

	public void setImei(String imei) {
		this.imei = imei;
	}

	public String getEquipoAsociado() {
		return equipoAsociado;
	}

	public void setEquipoAsociado(String equipoAsociado) {
		this.equipoAsociado = equipoAsociado;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getCustomerID() {
		return customerID;
	}

	public void setCustomerID(String customerID) {
		this.customerID = customerID;
	}

	public String getCuenta() {
		return cuenta;
	}

	public void setCuenta(String cuenta) {
		this.cuenta = cuenta;
	}

	public String getNombrePlan() {
		return nombrePlan;
	}

	public void setNombrePlan(String nombrePlan) {
		this.nombrePlan = nombrePlan;
	}

	public String getCodigoPlan() {
		return codigoPlan;
	}

	public void setCodigoPlan(String codigoPlan) {
		this.codigoPlan = codigoPlan;
	}

	public String getSimCard() {
		return simCard;
	}

	public void setSimCard(String simCard) {
		this.simCard = simCard;
	}

	public String getFechaRenovacion() {
		return fechaRenovacion;
	}

	public void setFechaRenovacion(String fechaRenovacion) {
		this.fechaRenovacion = fechaRenovacion;
	}

	public String getRoaming() {
		return roaming;
	}

	public void setRoaming(String roaming) {
		this.roaming = roaming;
	}

	public String getNumeroDocumento() {
		return numeroDocumento;
	}

	public void setNumeroDocumento(String numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}

	public String getAliasMovil() {
		return aliasMovil;
	}

	public void setAliasMovil(String aliasMovil) {
		this.aliasMovil = aliasMovil;
	}

	public DefaultServiceResponse getDefaultServiceResponse() {
		return defaultServiceResponse;
	}

	public void setDefaultServiceResponse(DefaultServiceResponse defaultServiceResponse) {
		this.defaultServiceResponse = defaultServiceResponse;
	}

}
