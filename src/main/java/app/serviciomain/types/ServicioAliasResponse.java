package app.serviciomain.types;

import app.serviciomain.bean.DefaultServiceResponse;

public class ServicioAliasResponse {

	protected DefaultServiceResponse defaultServiceResponse;
	private String servicioPrincipal;
	private String valorServicio;
	private String servicioAlias;
	private String numeroCuenta;
	private String descripcionCuenta;
	private String cuentaAlias;

	public String getNumeroCuenta() {
		return numeroCuenta;
	}

	public void setNumeroCuenta(String numeroCuenta) {
		this.numeroCuenta = numeroCuenta;
	}

	public String getDescripcionCuenta() {
		return descripcionCuenta;
	}

	public void setDescripcionCuenta(String descripcionCuenta) {
		this.descripcionCuenta = descripcionCuenta;
	}

	public String getCuentaAlias() {
		return cuentaAlias;
	}

	public void setCuentaAlias(String cuentaAlias) {
		this.cuentaAlias = cuentaAlias;
	}

	public DefaultServiceResponse getDefaultServiceResponse() {
		return defaultServiceResponse;
	}

	public void setDefaultServiceResponse(DefaultServiceResponse defaultServiceResponse) {
		this.defaultServiceResponse = defaultServiceResponse;
	}

	public String getServicioPrincipal() {
		return servicioPrincipal;
	}

	public void setServicioPrincipal(String servicioPrincipal) {
		this.servicioPrincipal = servicioPrincipal;
	}

	public String getValorServicio() {
		return valorServicio;
	}

	public void setValorServicio(String valorServicio) {
		this.valorServicio = valorServicio;
	}

	public String getServicioAlias() {
		return servicioAlias;
	}

	public void setServicioAlias(String servicioAlias) {
		this.servicioAlias = servicioAlias;
	}
}
