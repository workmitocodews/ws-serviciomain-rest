package app.serviciomain.types;

import java.util.List;
import app.serviciomain.bean.DefaultServiceResponse;
import app.serviciomain.bean.Parametro;

public class ParametroConsultaResponse extends DefaultServiceResponse {

	private List<Parametro> listaParametros;

	public List<Parametro> getListaParametros() {
		return listaParametros;
	}

	public void setListaParametros(List<Parametro> listaParametros) {
		this.listaParametros = listaParametros;
	}
}
