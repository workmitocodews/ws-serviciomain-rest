package app.serviciomain.types;

import java.util.List;
import app.serviciomain.bean.DefaultServiceResponse;
import app.serviciomain.bean.Favorito;

public class ConsultaFavoritoResponse extends DefaultServiceResponse {
	private List<Favorito> listaFavoritos;

	public List<Favorito> getListaFavoritos() {
		return listaFavoritos;
	}

	public void setListaFavoritos(List<Favorito> listaFavoritos) {
		this.listaFavoritos = listaFavoritos;
	}
}
