package app.serviciomain.types;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class ServicioRequestBase {
	@NotNull
	@Size(min = 1)
	protected String idTransaccion;
	@NotNull
	@Size(min = 1)
	protected String numeroDocumento;
	@NotNull
	@Size(min = 1)
	protected String tipoDocumento;

	public String getIdTransaccion() {
		return idTransaccion;
	}

	public void setIdTransaccion(String idTransaccion) {
		this.idTransaccion = idTransaccion;
	}

	public String getNumeroDocumento() {
		return numeroDocumento;
	}

	public void setNumeroDocumento(String numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}

	public String getTipoDocumento() {
		return tipoDocumento;
	}

	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}
}
