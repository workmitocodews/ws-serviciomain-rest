package app.serviciomain.types;

import java.util.List;

import app.serviciomain.bean.DefaultServiceResponse;
import app.serviciomain.bean.MovilLinea;

public class ServicioMovilLineasResponse {
	protected DefaultServiceResponse defaultServiceResponse;
	private List<MovilLinea> listaLineasMoviles;

	public DefaultServiceResponse getDefaultServiceResponse() {
		return defaultServiceResponse;
	}

	public void setDefaultServiceResponse(DefaultServiceResponse defaultServiceResponse) {
		this.defaultServiceResponse = defaultServiceResponse;
	}

	public List<MovilLinea> getListaLineasMoviles() {
		return listaLineasMoviles;
	}

	public void setListaLineasMoviles(List<MovilLinea> listaLineasMoviles) {
		this.listaLineasMoviles = listaLineasMoviles;
	}
}
