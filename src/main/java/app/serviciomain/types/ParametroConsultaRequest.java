package app.serviciomain.types;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class ParametroConsultaRequest extends ServicioRequestBase {
	@NotNull
	@Size(min = 1)
	private String codigopadre;

	public String getCodigoPadre() {
		return codigopadre;
	}

	public void setCodigoPadre(String codigopadre) {
		this.codigopadre = codigopadre;
	}
}
