package app.serviciomain.types;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

public class RegistraLineaFavoritaRequest extends ServicioRequestBase {
	@NotEmpty
	@Size(min = 1)
	protected String numeroCuenta;

	public String getNumeroCuenta() {
		return numeroCuenta;
	}

	public void setNumeroCuenta(String numeroCuenta) {
		this.numeroCuenta = numeroCuenta;
	}

	@NotEmpty
	@Size(min = 1)
	protected String numeroMovil;

	public String getNumeroMovil() {
		return numeroMovil;
	}

	public void setNumeroMovil(String numeroMovil) {
		this.numeroMovil = numeroMovil;
	}

}
