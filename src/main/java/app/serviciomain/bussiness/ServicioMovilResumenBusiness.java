package app.serviciomain.bussiness;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import app.serviciomain.exception.DBException;
import app.serviciomain.bean.DefaultServiceResponse;
import app.serviciomain.bean.MovilResumen;
import app.serviciomain.bean.MovilResumenResponse;
import app.serviciomain.bean.ServicioMovil;
import app.serviciomain.dao.BscsDao;
import app.serviciomain.types.ServicioMovilResumenRequest;
import app.serviciomain.types.ServicioMovilResumenResponse;
import app.serviciomain.util.Constantes;
import app.serviciomain.util.PropertiesExternos;
import app.serviciomain.util.UtilLog;

public class ServicioMovilResumenBusiness {

	@Inject
	BscsDao bscDao;

	public static final UtilLog utilLog = new UtilLog();

	public ServicioMovilResumenResponse servicioMovilResumen(ServicioMovilResumenRequest request)
			throws DBException {
		String nombreMetodo = "ServicioMovilResumenBusiness - " + Constantes.STR_CONSULTARRESUMENMOVIL;
		DefaultServiceResponse defaultServiceResponse = new DefaultServiceResponse();
		ServicioMovilResumenResponse response = new ServicioMovilResumenResponse();
		utilLog.inicioBusinessLog(request.getIdTransaccion(), nombreMetodo);
		MovilResumenResponse movilResumen = bscDao.consultaServiciosMovilesResumen(request);

		if (movilResumen.getCodigoRespuesta().equals(Constantes.CADENA_CERO)
			&& !movilResumen.getListaMovilResumen().isEmpty()) {
			response.setCustcode(movilResumen.getListaMovilResumen().get(0).getCustcode());
			response.setCustomerId(movilResumen.getListaMovilResumen().get(0).getCustomerId());
			response.setListaResumenMoviles(obtenerServiciosMovilesResumen(movilResumen.getListaMovilResumen()));
			defaultServiceResponse.setIdRespuesta(PropertiesExternos.CODIGO_EXITO);
			defaultServiceResponse.setMensaje(PropertiesExternos.MENSAJE_EXITO);
		} else {
			defaultServiceResponse.setIdRespuesta(PropertiesExternos.CODIGO_ERROR_IDF2);
			defaultServiceResponse.setMensaje(PropertiesExternos.MENSAJE_ERROR_IDF2);
		}

		utilLog.finallyBusinessLog(request.getIdTransaccion(), nombreMetodo);
		response.setDefaultServiceResponse(defaultServiceResponse);
		
		return response;
	}

	protected List<ServicioMovil> obtenerServiciosMovilesResumen(List<MovilResumen> listaMovilesDB) {
		List<ServicioMovil> serviciosMoviles = new ArrayList<>();
		for (MovilResumen movilDb : listaMovilesDB) {
			if (movilDb != null) {
				ServicioMovil servicioMovilNuevo = new ServicioMovil();
				servicioMovilNuevo.setDesServicio(movilDb.getDesServicio());
				servicioMovilNuevo.setFlag(movilDb.getFlag());
				servicioMovilNuevo.setTotalLineas(movilDb.getLineas());
				servicioMovilNuevo.setLineasActivas(movilDb.getLineasActivas());
				servicioMovilNuevo.setLineasAsociadas(movilDb.getLineasAsociadas());
				servicioMovilNuevo.setLineasBloqueadas(movilDb.getLineasBloqueadas());
				servicioMovilNuevo.setLineasSuspendidas(movilDb.getLineasSuspendidas());
				servicioMovilNuevo.setPlanes(movilDb.getPlanes());
				servicioMovilNuevo.setCicloFacturacion(movilDb.getCicloFacturacion());
				serviciosMoviles.add(servicioMovilNuevo);
			}
		}
		return serviciosMoviles;
	}

}
