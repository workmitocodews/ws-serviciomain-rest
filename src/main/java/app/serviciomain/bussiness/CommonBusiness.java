package app.serviciomain.bussiness;

import java.util.List;

import javax.inject.Inject;

import app.serviciomain.exception.DBException;
import app.serviciomain.exception.WSException;
import app.serviciomain.bean.Parametro;
import app.serviciomain.dao.CclDao;
import app.serviciomain.types.ParametroConsultaRequest;
import app.serviciomain.types.ParametroConsultaResponse;
import app.serviciomain.util.Constantes;
import app.serviciomain.util.PropertiesExternos;
import app.serviciomain.util.UtilLog;
import app.serviciomain.util.Utilitario;

public class CommonBusiness {
	
	@Inject
	public CclDao cclDao;
	
	public static final UtilLog utilLog = new UtilLog();
		
	public String homologaTipoDocumento(String strOrigenDato, String valor, String idTransaccion) {
		String valorBusqueda = strOrigenDato + Constantes.GUIONBAJO + valor;
		String codigoPadre = strOrigenDato + Constantes.GUIONBAJO + PropertiesExternos.STR_HOMOLOGA_TIPO_DOCUMENTO;
		ParametroConsultaResponse servicioParametro = new ParametroConsultaResponse();
		ParametroConsultaRequest request = new ParametroConsultaRequest();
		request.setCodigoPadre(codigoPadre);
		request.setIdTransaccion(idTransaccion);
		String documentoHomologado = "";
		try {
			servicioParametro = obtenerCatalogoServicio(request);
			if (servicioParametro.getIdRespuesta().equals(Constantes.CODIGO_EXITO)
					&& Utilitario.hayElementosEnLista(servicioParametro.getListaParametros())) {

				documentoHomologado = buscaCodigoHomologo(servicioParametro.getListaParametros(), valorBusqueda);

			}
		} catch (Exception e) {
			// LOGGER.info(e);
			utilLog.logErrorServicioLog(e, "homologaTipoDocumento()");
		}

		return documentoHomologado;
	}

	private String buscaCodigoHomologo(List<Parametro> listaParametro, String valorBusqueda) {
		String strCodigoHomologo = "";
		for (Parametro parametro : listaParametro) {
			if (valorBusqueda.equals(parametro.getCodigo())) {
				strCodigoHomologo = parametro.getValor();
			}
			;
		}
		return strCodigoHomologo;
	}

	private ParametroConsultaResponse obtenerCatalogoServicio(ParametroConsultaRequest request)
			throws DBException, WSException {

		ParametroConsultaResponse servicioParametro = new ParametroConsultaResponse();

		try {

			servicioParametro = cclDao.listarParametro(request);

		} catch (DBException dbe) {
			throw dbe;
		} catch (Exception e) {
			throw new WSException(PropertiesExternos.CODIGOERRORTECNICOWS, PropertiesExternos.ERRORTECNICOWS, e);
		}
		return servicioParametro;
	}


}
