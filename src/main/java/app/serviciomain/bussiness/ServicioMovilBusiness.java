package app.serviciomain.bussiness;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import app.serviciomain.bean.*;
import app.serviciomain.exception.DBException;
import app.serviciomain.exception.WSException;
import app.serviciomain.dao.BscsDao;
import app.serviciomain.dao.CclDao;
import app.serviciomain.types.DetalleMovilRequest;
import app.serviciomain.types.DetalleMovilResponse;
import app.serviciomain.types.ServicioAliasRequest;
import app.serviciomain.types.ServicioAliasResponse;
import app.serviciomain.types.ServicioMovilResponse;
import app.serviciomain.types.ServicioRequestBase;
import app.serviciomain.util.Constantes;
import app.serviciomain.util.PropertiesExternos;
import app.serviciomain.util.UtilLog;
import app.serviciomain.types.DetalleLineaResponse;
public class ServicioMovilBusiness {

	@Inject
	public BscsDao bscDao;

	@Inject
	public CclDao cclDao;

	public static final UtilLog utilLog = new UtilLog();
	private static final String STR_SERVICIOMOVILBUSINESS="ServicioMovilBusiness - ";
	public ServicioMovilResponse servicioMovil(ServicioRequestBase request) throws DBException {
		String nombreMetodo = STR_SERVICIOMOVILBUSINESS + Constantes.STR_CONSULTARSERVICIOMOVIL;
		DefaultServiceResponse defaultServiceResponse = new DefaultServiceResponse();
		ServicioMovilResponse response = new ServicioMovilResponse();
		utilLog.inicioBusinessLog(request.getIdTransaccion(), nombreMetodo);
		MovilResponse movilResponse = new MovilResponse();

		MovilResponse listarServicioMovil = bscDao.consultaServiciosMoviles(request);

		if (listarServicioMovil.getCodigoRespuesta().equals(Constantes.CADENA_CERO)
			&& listarServicioMovil.getListaCuentasMoviles() != null
			&& !listarServicioMovil.getListaCuentasMoviles().isEmpty()) {

			movilResponse.setTotalBolsas(listarServicioMovil.getTotalBolsas());
			movilResponse.setTotalCuentas(listarServicioMovil.getTotalCuentas());
			movilResponse.setTotalLineas(listarServicioMovil.getTotalLineas());
			movilResponse.setTotalPlanes(listarServicioMovil.getTotalPlanes());

			AliasResponse aliasCorporativo = cclDao.consultaAlias(request, Constantes.BUSQUEDA_MOVIL);
			movilResponse.setListaCuentasMoviles(
			obtenerListaMovil(listarServicioMovil.getListaCuentasMoviles(), aliasCorporativo));

			response.setMovilResponse(movilResponse);
			defaultServiceResponse.setIdRespuesta(PropertiesExternos.CODIGO_EXITO);
			defaultServiceResponse.setMensaje(PropertiesExternos.MENSAJE_EXITO);

		} else {
			defaultServiceResponse.setIdRespuesta(PropertiesExternos.CODIGO_ERROR_IDF2);
			defaultServiceResponse.setMensaje(PropertiesExternos.MENSAJE_ERROR_IDF2);
		}

		response.setDefaultServiceResponse(defaultServiceResponse);
		utilLog.finallyBusinessLog(request.getIdTransaccion(), nombreMetodo);

		return response;
	}

	private ArrayList<Movil> obtenerListaMovil(List<Movil> listaCuentasMoviles, AliasResponse aliasresponse) {
		ArrayList<Movil> listaMovil = new ArrayList<>();

		for (Movil cuentaMovil : listaCuentasMoviles) {
			cuentaMovil.setAlias(new Alias());
			cuentaMovil.getAlias().setCustcode(cuentaMovil.getCustcode().trim());
			cuentaMovil.getAlias().setAlias(Constantes.VACIO);
			cuentaMovil.getAlias().setDescripcion(Constantes.VACIO);

			if (aliasresponse.getCodigoRespuesta().equals(Constantes.CADENA_CERO)
					&& aliasresponse.getListaAliasCorporativo() != null
					&& !aliasresponse.getListaAliasCorporativo().isEmpty()) {

				Alias infoAlias = obtenerAliasDescripcion(cuentaMovil.getCustcode(),
						aliasresponse.getListaAliasCorporativo());
				if (infoAlias != null) {
					cuentaMovil.getAlias().setDescripcion(infoAlias.getDescripcion());
					cuentaMovil.getAlias().setAlias(infoAlias.getAlias());
				}
			}

			cuentaMovil.setTipoAlias((cuentaMovil.getCuentaPadre() == null || cuentaMovil.getCuentaPadre().isEmpty())
					? Constantes.TIPO_ALIAS_CUENTA
					: Constantes.TIPO_ALIAS_SUBCUENTA);

			listaMovil.add(cuentaMovil);
		}

		return listaMovil;
	}

	private Alias obtenerAliasDescripcion(String custcode, List<Alias> listaAlias) {
		Alias infoAlias = null;

		if (custcode != null) {
			for (Alias aliasCorp : listaAlias) {
				if (custcode.trim().equals(aliasCorp.getCustcode().trim())) {
					infoAlias = new Alias();
					infoAlias.setDescripcion(aliasCorp.getDescripcion());
					infoAlias.setAlias(aliasCorp.getAlias());
					break;
				}
			}
		}
		return infoAlias;
	}

	public DetalleMovilResponse servicioDetalleMovil(DetalleMovilRequest request) throws DBException, WSException {
		String nombreMetodo = STR_SERVICIOMOVILBUSINESS + Constantes.STR_CONSULTARDETALLEMOVIL;
		DetalleMovilResponse detalleMovilResponse = new DetalleMovilResponse();
		ServicioAliasResponse servicioAlias = new ServicioAliasResponse();

		try {
			utilLog.inicioBusinessLog(request.getIdTransaccion(), nombreMetodo);

			detalleMovilResponse = bscDao.consultaDetalleMovil(request);

			if (detalleMovilResponse.getDefaultServiceResponse().getIdRespuesta().equals(Constantes.CADENA_CERO)) {
				servicioAlias = obtenerAliasServicio(request);

				detalleMovilResponse.setAliasMovil(servicioAlias.getServicioAlias());
				detalleMovilResponse.setAliasCuenta(servicioAlias.getCuentaAlias());
				detalleMovilResponse.setCuenta(servicioAlias.getDescripcionCuenta());
			}
		} catch (DBException dbe) {
			throw dbe;
		} catch (Exception e) {
			throw new WSException(PropertiesExternos.CODIGOERRORTECNICOWS, PropertiesExternos.ERRORTECNICOWS, e);
		} finally {
			utilLog.finallyBusinessLog(request.getIdTransaccion(), nombreMetodo);
		}
		return detalleMovilResponse;
	}
	public DetalleLineaResponse servicioDetalleLinea(DetalleMovilRequest request) throws DBException, WSException {
		String nombreMetodo = STR_SERVICIOMOVILBUSINESS + Constantes.STR_CONSULTARDETALLEMOVIL;
		DetalleLineaResponse detalleLineaResponse = new DetalleLineaResponse();
		ServicioAliasResponse servicioAlias = new ServicioAliasResponse();

		try {
			utilLog.inicioBusinessLog(request.getIdTransaccion(), nombreMetodo);
			detalleLineaResponse= bscDao.consultaDetalleLinea(request);
			
			if (detalleLineaResponse.getDefaultServiceResponse().getIdRespuesta().equals(Constantes.CADENA_CERO)) {
				servicioAlias = obtenerAliasServicio(request);

				detalleLineaResponse.setAliasMovil(servicioAlias.getServicioAlias());
				detalleLineaResponse.setAliasCuenta(servicioAlias.getCuentaAlias());
				detalleLineaResponse.setCuenta(servicioAlias.getDescripcionCuenta());
			}
			
			DetalleMovilResponse detalleMovil =new DetalleMovilResponse();
			detalleMovil = bscDao.consultaDetalleMovil(request);
			if (detalleMovil.getDefaultServiceResponse().getIdRespuesta().equals(Constantes.CADENA_CERO)) {
				detalleLineaResponse.setCuenta(detalleMovil.getCuenta());
				detalleLineaResponse.setCodigoPlan(detalleMovil.getCodigoPlan());
				detalleLineaResponse.setNombrePlan(detalleMovil.getNombrePlan());
				detalleLineaResponse.setFechaRenovacion(detalleMovil.getFechaRenovacion());
				detalleLineaResponse.setEstado(detalleMovil.getEstado());
				detalleLineaResponse.setCustomerID(detalleMovil.getCustomerID());
			}
			
		} catch (DBException dbe) {
			throw dbe;
		} catch (Exception e) {
			throw new WSException(PropertiesExternos.CODIGOERRORTECNICOWS, PropertiesExternos.ERRORTECNICOWS, e);
		} finally {
			utilLog.finallyBusinessLog(request.getIdTransaccion(), nombreMetodo);
		}
		return detalleLineaResponse;
	}
	
	public ServicioAliasResponse obtenerAliasServicio(DetalleMovilRequest request) throws DBException, WSException {
		ServicioAliasResponse servicioAlias = new ServicioAliasResponse();
		ServicioAliasRequest requestAlias = new ServicioAliasRequest();
		try {
			requestAlias.setTipoDocumento(request.getTipoDocumento());
			requestAlias.setNumeroDocumento(request.getNumeroDocumento());
			requestAlias.setTipoAlias(Constantes.TIPO_ALIAS_MOVIL);
			requestAlias.setValorServicio(Constantes.CODIGO_PERU_NUMERO + request.getValorServicio());

			servicioAlias = cclDao.consultaServicioAlias(requestAlias);

		} catch (DBException dbe) {
			throw dbe;
		} catch (Exception e) {
			throw new WSException(PropertiesExternos.CODIGOERRORTECNICOWS, PropertiesExternos.ERRORTECNICOWS, e);
		}

		return servicioAlias;
	}

}
