package app.serviciomain.bussiness;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import app.serviciomain.exception.WSException;
import app.serviciomain.bean.DefaultServiceResponse;
import app.serviciomain.bean.Favorito;
import app.serviciomain.dao.BscsDao;
import app.serviciomain.types.ServicioRequestBase;
import app.serviciomain.types.ConsultaFavoritoResponse;
import app.serviciomain.types.DetalleMovilRequest;
import app.serviciomain.types.RegistraLineaFavoritaRequest;
import app.serviciomain.types.ServicioAliasResponse;
import app.serviciomain.util.Constantes;
import app.serviciomain.util.PropertiesExternos;
import app.serviciomain.util.UtilLog;
import app.serviciomain.util.Utilitario;

public class FavoritoBusiness extends CommonBusiness {
	@Inject
	BscsDao bscDao;

	@Inject
	ServicioMovilBusiness servicioMovil;

	public static final UtilLog utilLog = new UtilLog();

	public ConsultaFavoritoResponse listaFavorito(ServicioRequestBase request) throws WSException {
		String nombreMetodo = "FavoritoBusiness - " + Constantes.STR_CONSULTAFAVORITO;
		String tipoDocumentoOriginal = request.getTipoDocumento();
		ConsultaFavoritoResponse response = new ConsultaFavoritoResponse();
		try {
			utilLog.inicioBusinessLog(request.getIdTransaccion(), nombreMetodo);

			if (!(Utilitario.isNullOrEmpty(request.getNumeroDocumento())
					|| Utilitario.isNullOrEmpty(request.getTipoDocumento()))) {
				String tipoDocumento = homologaTipoDocumento(PropertiesExternos.DBBSCS_NOMBRE,
						request.getTipoDocumento(), request.getIdTransaccion());
				utilLog.printStringLogInfo("tipoDocumento: " + tipoDocumento);
				request.setTipoDocumento(tipoDocumento);
				response = bscDao.consultaFavorito(request);

				if (Utilitario.hayElementosEnLista(response.getListaFavoritos())) {
					request.setTipoDocumento(tipoDocumentoOriginal);
					response.setListaFavoritos(agregarFavoritos(request, response.getListaFavoritos()));
				}

			} else {

				response.setIdRespuesta(PropertiesExternos.CODIGO_ERROR_IDF1);
				response.setMensaje(PropertiesExternos.MENSAJE_ERROR_IDF1);
				response.setIdTransaccional(request.getIdTransaccion());

			}

		} catch (Exception e) {
			throw new WSException(PropertiesExternos.CODIGOERRORTECNICOWS, PropertiesExternos.ERRORTECNICOWS, e);
		} finally {
			utilLog.finallyBusinessLog(request.getIdTransaccion(), nombreMetodo);
			// response.setDefaultServiceResponse(defaultServiceResponse);
		}
		return response;
	}

	private String verificaAlias(ServicioRequestBase request, String numeroLinea) {
		String strAlias = "";
		DetalleMovilRequest requestDetalle = new DetalleMovilRequest();
		requestDetalle.setNumeroDocumento(request.getNumeroDocumento());
		requestDetalle.setTipoDocumento(request.getTipoDocumento());
		requestDetalle.setValorServicio(numeroLinea);
		ServicioAliasResponse servicioAlias = new ServicioAliasResponse();
		try {
			servicioAlias = servicioMovil.obtenerAliasServicio(requestDetalle);
			if (servicioAlias.getDefaultServiceResponse().getIdRespuesta().equals(Constantes.CODIGO_EXITO)) {
				strAlias = servicioAlias.getServicioAlias();
			}
		} catch (Exception e) {
			// LOGGER.info(e);
			utilLog.logErrorServicioLog(e, "verificaAlias()");
		}
		return strAlias;
	}

	private List<Favorito> agregarFavoritos(ServicioRequestBase request, List<Favorito> listaFavorito) {
		List<Favorito> newlistaFavorito = new ArrayList<>();
		Favorito newFavorito = new Favorito();
		for (Favorito favorito : listaFavorito) {

			newFavorito.setCuenta(favorito.getCuenta());
			newFavorito.setSubcuenta(favorito.getSubcuenta());
			newFavorito.setLinea(favorito.getLinea());

			newFavorito.setAlias(verificaAlias(request, favorito.getLinea()));

			newlistaFavorito.add(newFavorito);
			newFavorito = new Favorito();
		}
		return newlistaFavorito;
	}

	public DefaultServiceResponse registraLineaFavorita(RegistraLineaFavoritaRequest request) throws WSException {
		String nombreMetodo = "FavoritoBusiness - " + Constantes.STR_REGISTRALINEAFAVORITA;
		DefaultServiceResponse response = new DefaultServiceResponse();
		try {
			utilLog.inicioBusinessLog(request.getIdTransaccion(), nombreMetodo);

			if (!(Utilitario.isNullOrEmpty(request.getNumeroDocumento())
					|| Utilitario.isNullOrEmpty(request.getTipoDocumento()))) {
				String tipoDocumento = homologaTipoDocumento(PropertiesExternos.DBBSCS_NOMBRE,
						request.getTipoDocumento(), request.getIdTransaccion());
				utilLog.printStringLogInfo("tipoDocumento: " + tipoDocumento);
				request.setTipoDocumento(tipoDocumento);

				response = bscDao.registraLineaFavorita(request);
			} else {
				response.setIdRespuesta(PropertiesExternos.CODIGO_ERROR_IDF1);
				response.setMensaje(PropertiesExternos.MENSAJE_ERROR_IDF1);
				response.setIdTransaccional(request.getIdTransaccion());
			}

		} catch (Exception e) {
			throw new WSException(PropertiesExternos.CODIGOERRORTECNICOWS, PropertiesExternos.ERRORTECNICOWS, e);
		} finally {
			utilLog.finallyBusinessLog(request.getIdTransaccion(), nombreMetodo);
		}
		return response;
	}
}
