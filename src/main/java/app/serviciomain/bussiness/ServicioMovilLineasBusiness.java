package app.serviciomain.bussiness;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import app.serviciomain.bean.*;
import app.serviciomain.exception.DBException;
import app.serviciomain.exception.WSException;
import app.serviciomain.dao.BscsDao;
import app.serviciomain.types.ServicioMovilLineasResponse;
import app.serviciomain.types.ServicioMovilResumenRequest;
import app.serviciomain.util.Constantes;
import app.serviciomain.util.PropertiesExternos;
import app.serviciomain.util.UtilLog;

public class ServicioMovilLineasBusiness extends CommonBusiness{

	@Inject
	BscsDao bscDao;

	public static final UtilLog utilLog = new UtilLog();

	public ServicioMovilLineasResponse servicioMovilLineas(ServicioMovilResumenRequest request)
			throws DBException, WSException {
		String nombreMetodo = "ServicioMovilLineasBusiness - " + Constantes.STR_CONSULTARLINEASMOVILES;
		DefaultServiceResponse defaultServiceResponse = new DefaultServiceResponse();
		ServicioMovilLineasResponse response = new ServicioMovilLineasResponse();
		String tipoDocumentoCcl = request.getTipoDocumento();
		try {
			utilLog.inicioBusinessLog(request.getIdTransaccion(), nombreMetodo);

			String tipoDocumento = homologaTipoDocumento(PropertiesExternos.DBBSCS_NOMBRE, request.getTipoDocumento(),
					request.getIdTransaccion());
			utilLog.printStringLogInfo("tipoDocumento: " + tipoDocumento);
			request.setTipoDocumento(tipoDocumento);

			MovilLineaResponse movilLinea = bscDao.consultaServiciosMovilesLineas(request);

			if (movilLinea.getCodigoRespuesta().equals(Constantes.CADENA_CERO)
					&& !movilLinea.getListaMovilLinea().isEmpty()) {

				response.setListaLineasMoviles(obtenerServiciosLineasMoviles(movilLinea.getListaMovilLinea()));

				FavoritaLineaResponse favoritaLinea = bscDao.consultaObtenerFavorito(request);

				if (favoritaLinea.getCodigoRespuesta().equals(Constantes.CADENA_CERO)
						&& !favoritaLinea.getListaFavoritaLineaCorporativo().isEmpty()) {

					response.setListaLineasMoviles(obtenerServiciosLineasMovilesFavorito(
							response.getListaLineasMoviles(), favoritaLinea.getListaFavoritaLineaCorporativo()));
				}

				request.setTipoDocumento(tipoDocumentoCcl);
				AliasLineaResponse aliasLinea = cclDao.consultaObtenerAlias(request);

				if (aliasLinea.getCodigoRespuesta().equals(Constantes.CADENA_CERO)
						&& !aliasLinea.getListaAliasLineaCorporativo().isEmpty()) {

					response.setListaLineasMoviles(obtenerServiciosLineasMovilesAlias(response.getListaLineasMoviles(),
							aliasLinea.getListaAliasLineaCorporativo()));
				}

				defaultServiceResponse.setIdRespuesta(PropertiesExternos.CODIGO_EXITO);
				defaultServiceResponse.setMensaje(PropertiesExternos.MENSAJE_EXITO);
			} else {
				defaultServiceResponse.setIdRespuesta(PropertiesExternos.CODIGO_ERROR_IDF2);
				defaultServiceResponse.setMensaje(PropertiesExternos.MENSAJE_ERROR_IDF2);
			}
		} catch (DBException dbe) {
			throw dbe;
		} catch (Exception e) {
			throw new WSException(PropertiesExternos.CODIGOERRORTECNICOWS, PropertiesExternos.ERRORTECNICOWS, e);
		} finally {
			utilLog.finallyBusinessLog(request.getIdTransaccion(), nombreMetodo);
			response.setDefaultServiceResponse(defaultServiceResponse);
		}
		return response;
	}


	protected List<MovilLinea> obtenerServiciosLineasMoviles(List<MovilLinea> listaMovilesDB) {
		List<MovilLinea> serviciosMoviles = new ArrayList<>();

		for (MovilLinea movilDb : listaMovilesDB) {
			if (movilDb != null) {
				MovilLinea servicioMovilNuevo = new MovilLinea();
				servicioMovilNuevo.setBloqueSuspendido(movilDb.getBloqueSuspendido());
				servicioMovilNuevo.setBolsa(movilDb.getBolsa());
				servicioMovilNuevo.setContrato(movilDb.getContrato());
				servicioMovilNuevo.setFavorita(Constantes.CADENA_CERO);
				switch (movilDb.getEstadoContrato()) {
				case "a":
            servicioMovilNuevo.setEstadoContrato("Activa");
            break;
				case "s":
            servicioMovilNuevo.setEstadoContrato("Suspendida");
            break;
				default:
            break;
				}
				servicioMovilNuevo.setLinea(movilDb.getLinea());
				servicioMovilNuevo.setOfertaProducto(movilDb.getOfertaProducto());
				servicioMovilNuevo.setTipoPlan(movilDb.getTipoPlan());

				serviciosMoviles.add(servicioMovilNuevo);
			}
		}
		return serviciosMoviles;
	}

	protected List<MovilLinea> obtenerServiciosLineasMovilesAlias(List<MovilLinea> listaMovilesDB,
			List<AliasLinea> listaAliasDB) {
		for (MovilLinea movilDb : listaMovilesDB) {
      for(AliasLinea aliasLinea : listaAliasDB) {		
        if(aliasLinea.getLineaMovil().contains(movilDb.getLinea())) { 
          movilDb.setAlias(aliasLinea.getAliasAsociado());
          break;
		 }else {
          movilDb.setAlias(Constantes.CADENA_VACIA);
         }
		}
		}
		return listaMovilesDB;
	}

	protected List<MovilLinea> obtenerServiciosLineasMovilesFavorito(List<MovilLinea> listaMovilesDB,
			List<FavoritaLinea> listaFavoritaDB) {

		for (MovilLinea movilDb : listaMovilesDB) {
      for(FavoritaLinea favoritaLinea : listaFavoritaDB) {			
        if(favoritaLinea.getLinea().equalsIgnoreCase(movilDb.getLinea())){
          movilDb.setFavorita(Constantes.CADENA_UNO);
          break;
				};
			}
		}
		return listaMovilesDB;
	}
}
