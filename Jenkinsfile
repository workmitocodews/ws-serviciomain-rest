pipeline {
   agent none
    stages {
        stage("Build") {
            agent {
                docker {
                    image 'maven:3.6.3-jdk-8'
                }
            }
            steps {
                sh "mvn clean package -B -ntp"
            }
        }
        stage("Sonar") {
            agent {
                 docker {
                      image 'maven:3.6.3-jdk-8'
                 }
            }
            steps {
                withCredentials([[$class: 'FileBinding', credentialsId: 'sonarqube-settings', variable: 'M2_SETTINGS']]) {
                   sh "mvn sonar:sonar -B -ntp -s ${M2_SETTINGS}"
               }
            }
        }
        stage('CoverageReport') {
           agent {
                docker {
                    image 'maven:3.6.3-jdk-8'
                }
           }
            steps {
               step([
                   $class: 'JacocoPublisher',
                   execPattern: 'target/*.exec',
                   classPattern: 'target/classes',
                   sourcePattern: 'src/main/Java',
                   exclusionPattern: 'src/test*'
               ])
           }
        }
        stage("Artifactory") {
            agent {
                docker {
                    image 'maven:3.6.3-jdk-8'
                }
            }
            steps {
               script{
                     def server = Artifactory.server 'artifactory'
                     def repository = "ws-serviciomain-rest"

                     if("${GIT_BRANCH}" == "master"){
                         repository = "${repository}-release/com/peru/ws-serviciomain-rest/1.0.0/"
                     } else {
                         repository = "${repository}-snapshot/com/peru/ws-serviciomain-rest/1.0.0-SNAPSHOT-${BUILD_NUMBER}/"
                     }

                    def uploadSpec = """
                        {
                            "files": [
                                {
                                    "pattern": "target/.*.war",
                                    "target": "${repository}",
                                    "regexp": "true"
                                }
                            ]
                        }
                    """
                    def buildInfo = Artifactory.newBuildInfo()
                    buildInfo.env.capture = true

                    buildInfo.name = "ws-serviciomain-rest"
                    buildInfo.number = "${BUILD_NUMBER}"

                    server.upload spec: uploadSpec, buildInfo: buildInfo
                    server.publishBuildInfo buildInfo
               }
            }
        }

        stage("Approval") {
                    steps {
                        script {
                             def users = "erika.collantes"
                               timeout(time: 1, unit: 'MINUTES') {
                                userInput = input(
                                    submitterParameter: 'approval',
                                    submitter: "${users}",
                                    message: "¿Desplegar en JBoss?", parameters: [
                                    [$class: 'BooleanParameterDefinition', defaultValue: true, description: '', name: 'Aprobar']
                                ])
                            }
                        }
                    }
        }
        stage("Deploy on JBoss 7.2") {
                    agent any
                    steps {
                        script {
                            echo "${userInput}"
                            if(userInput.Aprobar == true) {
                                 // notifique email
                                 // Enviar al artefacto a centos 8.2
                                 sshagent ( credentials: ['jenkins-ssh-privatekey']){
                                   sh """
                                        scp -o StrictHostKeyChecking=no target/serviciomain.war root@165.227.88.63:/root
                                        ssh root@165.227.88.63 '~/EAP-7.3.0/bin/jboss-cli.sh -c --command="undeploy /root/serviciomain.war"'
                                        ssh root@165.227.88.63 '~/EAP-7.3.0/bin/jboss-cli.sh -c --command="deploy /root/serviciomain.war"'

                                     """
                                }
                            }
                        }
                    }
        }
    }
}